package com.deltatechnepal.deltaezinventory.main;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.deltatechnepal.deltaezinventory.R;
import com.deltatechnepal.deltaezinventory.adapter.ItemsAdapter;
import com.deltatechnepal.deltaezinventory.helper.CustomToast;
import com.deltatechnepal.deltaezinventory.helper.Iso2Phone;
import com.deltatechnepal.deltaezinventory.helper.SharedPreManager;
import com.deltatechnepal.deltaezinventory.model.Item;
import com.deltatechnepal.deltaezinventory.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.TimeUnit;


public class LoginActivity extends AppCompatActivity {
    private Context mContext;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private FirebaseAuth mAuth;
    ImageView mobile;
    Button button;
    EditText editBox;
    TextInputLayout textInputLayout;
    FloatingActionButton fabbutton;
    ProgressDialog progressDialog;
    String mVerificationId;
    Boolean mVerified = false;
    private DatabaseReference mAllowedNumbers;
    private FirebaseDatabase mFirebaseInstance;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    CustomToast cToast;
    long number;
    TextView tvRetry;
    PhoneAuthProvider.ForceResendingToken retryToken;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //setting toolbar and actionbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        mContext = this;
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);

        cToast=new CustomToast();
        tvRetry=findViewById(R.id.tvRetry);
        textInputLayout = findViewById(R.id.holder1);
        editBox = findViewById(R.id.mobile_number);
        mobile=findViewById(R.id.mobile);
        button=findViewById(R.id.button);


        //Check if ALready Logged In
        if(SharedPreManager.getInstance(mContext).isLoggedIn()) {

            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }










        //Firebase Auth Object and Related Callbacks
        mAuth = FirebaseAuth.getInstance();

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verificaiton without
                //     user action.
                Log.d("TAG", "onVerificationCompleted:" + credential);
                mVerified=true;
                signInWithPhoneAuthCredential(credential);

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w("TAG", "onVerificationFailed", e);

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    Toast.makeText(getApplicationContext(),"Verification Failed!! Invalid Verification Code", Toast.LENGTH_LONG).show();

                }
                else if (e instanceof FirebaseTooManyRequestsException) {
                    Toast.makeText(getApplicationContext(),"Verification Failed!! SMS  Quota Reached", Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onCodeSent(String verificationId,
                                   final PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d("TAG", "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;
                mVerified=true;
                mobile.setImageDrawable(getResources().getDrawable(R.drawable.enter_code));
                editBox.getText().clear();
                textInputLayout.setHint("Enter Code");
                button.setText("Verify");
                progressDialog.dismiss();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                       retryVisible(token);
                    }
                }, 120000);

            }
        };
    }


    public void SignIn(View v){
        final List<Long> numbers=new ArrayList<>();
        number=Long.valueOf(editBox.getText().toString().trim());

        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Checking.Please Wait.....");
        progressDialog.setCancelable(false);
        progressDialog.setTitle(null);
        progressDialog.show();
        mFirebaseInstance = FirebaseDatabase.getInstance();

         // get reference to 'users' node
        mAllowedNumbers = mFirebaseInstance.getReference().child("allowedNumbers");

        mAllowedNumbers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                numbers.clear();
                for (DataSnapshot postSnapshot: snapshot.getChildren()) {

                    long no=postSnapshot.getValue(long.class);
                    numbers.add(no);

                    // here you can access to name property like university.name

                }

                if(numbers.contains(number)) {
                    progressDialog.dismiss();
                    startSignIn();

                }
                else{
                    progressDialog.dismiss();

                    cToast.showToast(LoginActivity.this,
                            getResources().getDrawable(R.drawable.ic_info_white),
                            "Not Allowed!");
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getMessage());
            }
        });





    }



    public void retryVisible(PhoneAuthProvider.ForceResendingToken token){

        tvRetry.setVisibility(View.VISIBLE);
        retryToken=token;

    }

    public void retrySubmit(View v){


     resendVerificationCode(String.valueOf(number),retryToken);



    }




    public void startSignIn(){



        if (button.getText().toString().equalsIgnoreCase("Submit")) {
            if (!editBox.getText().toString().trim().isEmpty() && editBox.getText().toString().trim().length() >= 10) {
                progressDialog = new ProgressDialog(LoginActivity.this);
                progressDialog.setMessage("In Progress.Please Wait.....");
                progressDialog.setCancelable(false);
                progressDialog.setTitle(null);
                progressDialog.show();

                TelephonyManager telephonyManager = (TelephonyManager)getSystemService(getApplicationContext().TELEPHONY_SERVICE);
                String countryCode = telephonyManager.getNetworkCountryIso();
                String phoneCode = Iso2Phone.getPhone(countryCode);
                String mobileNumber = phoneCode+editBox.getText().toString().trim();
                startPhoneNumberVerification(mobileNumber);
                mVerified = false;

            }
            else {
                editBox.setError("Please enter valid mobile number");
            }
        }

        if (button.getText().toString().equalsIgnoreCase("Verify")) {
            if (!editBox.getText().toString().trim().isEmpty() && mVerified) {

                Toast.makeText(getApplicationContext(),"Please Wait.......", Toast.LENGTH_LONG).show();

                PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, editBox.getText().toString().trim());
                signInWithPhoneAuthCredential(credential);
            }
        }
    }


    private void startPhoneNumberVerification(String phoneNumber) {
        // [START start_phone_auth]
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                120,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]

    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                120,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks

        Toast.makeText(mContext,"Resent Code",Toast.LENGTH_SHORT).show();
        tvRetry.setVisibility(View.GONE);
    }


    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("TAG", "signInWithCredential:success");

                            FirebaseUser data = task.getResult().getUser();

                            User user=new User(data.getUid(),data.getPhoneNumber());

                            mVerified = true;
                            mobile.setImageDrawable(getResources().getDrawable(R.drawable.mobile_verified));
                            editBox.setEnabled(false);
                            cToast.showToast(LoginActivity.this,
                                    getResources().getDrawable(R.drawable.ic_done),
                                    "Successfully Verified.Welcome!");
                            if (mVerified) {
                                SharedPreManager.getInstance(mContext).userLogin(user);
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                finish();
                            }

                            // ...
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w("TAG", "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                cToast.showToast(LoginActivity.this,
                                        getResources().getDrawable(R.drawable.ic_info_white),
                                        "Invalid OTP !Please enter correct Code");


                            }
                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}
