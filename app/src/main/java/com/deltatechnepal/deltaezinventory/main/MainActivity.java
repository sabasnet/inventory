package com.deltatechnepal.deltaezinventory.main;


import android.Manifest;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.deltatechnepal.deltaezinventory.R;
import com.deltatechnepal.deltaezinventory.adapter.DashMenuAdapter;
import com.deltatechnepal.deltaezinventory.billing.BillingManager;
import com.deltatechnepal.deltaezinventory.billing.BillingProvider;
import com.deltatechnepal.deltaezinventory.helper.MConstant;
import com.deltatechnepal.deltaezinventory.helper.SharedPreManager;
import com.deltatechnepal.deltaezinventory.model.DashMenu;

import java.util.ArrayList;
import java.util.List;

import com.deltatechnepal.deltaezinventory.skulist.AcquireFragment;


public class MainActivity extends AppCompatActivity implements BillingProvider {
    DrawerLayout mDrawerLayout;
    private Context mContext;
    DashMenuAdapter dmAdapter;
    int clickedItem;
    private BillingManager mBillingManager;
    private AcquireFragment mAcquireFragment;
    private static final String DIALOG_TAG = "dialog";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //setting toolbar and actionbar
        mContext = this;

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_drawer);

        // Try to restore dialog fragment if we were showing it prior to screen rotation
        if (savedInstanceState != null) {
            mAcquireFragment = (AcquireFragment) getSupportFragmentManager()
                    .findFragmentByTag(DIALOG_TAG);
        }

        // Create and initialize BillingManager which talks to BillingLibrary
        mBillingManager = new BillingManager(this);


        mDrawerLayout = findViewById(R.id.drawer_layout);


        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        mDrawerLayout.closeDrawers();

                        /*menuItem.setChecked(true);*/
                        // close drawer when item is tapped


                        switch (menuItem.getItemId()){
                            case R.id.nav_logout: {
                                clickedItem=0;
                                new AlertDialog.Builder(mContext)
                                        .setTitle(getString(R.string.app_name))
                                        .setMessage("Want To Logout?")
                                        .setIcon(R.drawable.ic_info)
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                SharedPreManager.getInstance(mContext).logout();
                                                ActivityOptions options =
                                                        ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_right, R.anim.slide_left);
                                                Intent intent = new Intent(mContext, LoginActivity.class);
                                                startActivity(intent, options.toBundle());
                                                finish();
                                            }})
                                        .setNegativeButton("No", null).show();


                                break;
                            }

                            case R.id.nav_create_item: {
                                clickedItem=R.id.nav_create_item;
                                break;
                            }

                            case R.id.nav_items: {
                                clickedItem=R.id.nav_items;
                                break;
                            }

                            case R.id.nav_purchase: {
                                clickedItem=R.id.nav_purchase;
                                break;
                            }
                            case R.id.nav_sales: {
                                clickedItem=R.id.nav_sales;
                                break;
                            }

                            case R.id.nav_upgrade: {
                                clickedItem=R.id.nav_upgrade;
                                break;
                            }

                            case R.id.nav_aboutus: {
                                clickedItem=R.id.nav_aboutus;
                                break;
                            }


                        }




                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        return true;
                    }
                });

        View navHeader = navigationView.getHeaderView(0);
        ImageView profile=navHeader.findViewById(R.id.userImage);
        Glide.with(this).load(this.getResources().getDrawable(R.drawable.logo)).apply(RequestOptions.circleCropTransform()).into(profile);

        TextView userEmail = navHeader.findViewById(R.id.userEmail);
        userEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(MConstant.DEVELOPER_URL)));
            }
        });



        mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                //Called when a drawer's position changes.
            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                // Called when a drawer has settled in a completely closed state.
                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_right, R.anim.slide_left);

                switch (clickedItem){

                    case R.id.nav_create_item: {
                        Intent intent = new Intent(mContext, ItemAddActivity.class);
                        startActivity(intent, options.toBundle());
                        break;
                    }


                    case R.id.nav_items: {
                        Intent intent = new Intent(mContext, ItemsActivity.class);
                         startActivity(intent, options.toBundle());
                        break;
                    }

                    case R.id.nav_purchase: {
                        Intent intent = new Intent(mContext, PurchaseActivity.class);
                        startActivity(intent, options.toBundle());
                        break;
                    }
                    case R.id.nav_sales: {
                        Intent intent = new Intent(mContext, SalesActivity.class);
                         startActivity(intent, options.toBundle());
                        break;
                    }

                    case R.id.nav_upgrade: {
                        //showPurchaseDialog(drawerView);// Using AlertDialog
                        showPurchaseDialog();

                        break;
                    }

                    case R.id.nav_aboutus: {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(MConstant.DEVELOPER_URL)));
                        break;
                    }

                }

                clickedItem=0;


            }

            @Override
            public void onDrawerStateChanged(int newState) {
                // Called when the drawer motion state changes. The new state will be one of STATE_IDLE, STATE_DRAGGING or STATE_SETTLING.
            }
        });


        setgvDash();

        //region Permissions
        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE,Manifest.permission.READ_CALENDAR,Manifest.permission.READ_CALENDAR}, 101);
        //endregion



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBillingManager.destroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);



                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.app_name))
                    .setMessage("Want To Exit?")
                    .setIcon(R.drawable.ic_info)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            finish();
                        }})
                    .setNegativeButton("No", null).show();
        }
    }

    @Override
    public BillingManager getBillingManager() {
        return mBillingManager;
    }

    private void setgvDash(){
        GridView gvDash = findViewById(R.id.gvDash);
        final List<DashMenu> menuList = new ArrayList<DashMenu>();

        DashMenu items = new DashMenu("Items","ItemsActivity",R.drawable.ic_items);
        menuList.add(items);

        DashMenu purchase = new DashMenu("Stock In","PurchaseActivity",R.drawable.ic_purchase);
        menuList.add(purchase);

        DashMenu sales = new DashMenu("Stock Out","SalesActivity",R.drawable.ic_sales);
        menuList.add(sales);

        dmAdapter = new DashMenuAdapter(mContext,menuList);
        gvDash.setAdapter(dmAdapter);
        gvDash.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                String activityToStart = "com.deltatechnepal.deltaezinventory.main."+menuList.get(position).activity;

                try {
                    Class<?> c = Class.forName(activityToStart);


                    Intent intent = new Intent(mContext, c);
                    ActivityOptions options =
                            ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_right, R.anim.slide_left);
                    startActivity(intent, options.toBundle());
                } catch(ClassNotFoundException ignored){
                    Log.i("Ignored", ignored.toString());
                }

            }
        });
    }

    public void showPurchaseDialog(View v) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_subscribe_premium_package, null);
        dialogBuilder.setView(dialogView);

        final ImageButton ibClose = (ImageButton) dialogView.findViewById(R.id.ibClose);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        alertDialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_background);
        alertDialog.show();

        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        dialogBuilder.setMessage(null);
    }


    public void showPurchaseDialog() {

        if (mAcquireFragment == null) {
            mAcquireFragment = new AcquireFragment();
        }

        if (!isAcquireFragmentShown()) {
            mAcquireFragment.show(getSupportFragmentManager(), DIALOG_TAG);
        }
    }

    public boolean isAcquireFragmentShown() {
        return mAcquireFragment != null && mAcquireFragment.isVisible();
    }

}