package com.deltatechnepal.deltaezinventory.main;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.deltatechnepal.deltaezinventory.R;
import com.deltatechnepal.deltaezinventory.adapter.SalesListAdapter;
import com.deltatechnepal.deltaezinventory.adapter.SoldCartAdapter;
import com.deltatechnepal.deltaezinventory.helper.SharedPreManager;
import com.deltatechnepal.deltaezinventory.model.Item;
import com.deltatechnepal.deltaezinventory.model.Purchase;
import com.deltatechnepal.deltaezinventory.model.PurchaseCart;
import com.deltatechnepal.deltaezinventory.model.Sales;
import com.deltatechnepal.deltaezinventory.model.SalesCart;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import android.util.Log;


public class SalesActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{
    private Context mContext;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvSales;
    private LinearLayoutManager linearLayoutManager;
    private SalesListAdapter adapter;
    private List<Sales> salesList=new ArrayList<>();
    private List<SalesCart> salesCart=new ArrayList<>();
    private List<Item> itemList = new ArrayList<>();
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    String number;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales);
        //setting toolbar and actionbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        mContext = this;
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);

        fab=findViewById(R.id.fabAdd);

        rvSales =  findViewById(R.id.rvSales);
        linearLayoutManager =
                new LinearLayoutManager(mContext);
        rvSales.setLayoutManager(linearLayoutManager);
        rvSales.setHasFixedSize(true);
        rvSales.setNestedScrollingEnabled(true);

        final Animation animationLeft= AnimationUtils.loadAnimation(mContext,
                R.anim.s_left);
        animationLeft.setDuration(500);
        final Animation animationRight= AnimationUtils.loadAnimation(mContext,
                R.anim.s_right);
        animationRight.setDuration(500);

        rvSales.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {


                if (linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                    //this is the top of the RecyclerView
                    fab.setVisibility(View.VISIBLE);
                    fab.startAnimation(animationLeft);
                }
                else{
                    if(fab.getVisibility()==View.VISIBLE){
                        fab.setVisibility(View.GONE);
                        fab.startAnimation(animationRight);
                    }
                }



            }
        });



        swipeRefreshLayout =findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setRefreshing(true);



        swipeRefreshLayout.setOnRefreshListener(this);

        fetchItems();
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {


            }
        });






    }


    /**
     * This method is called when swipe refresh is pulled down
     */
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        fetchItems();
        swipeRefreshLayout.setRefreshing(false);

    }


    public void fetchItems()
    {
        number= SharedPreManager.getInstance(mContext).getUser().getMobileNumber();
        //Firebase
        mFirebaseInstance = FirebaseDatabase.getInstance();

        // get reference to 'users' node
        mFirebaseDatabase = mFirebaseInstance.getReference
                (number+"/Book/Sales/");


        mFirebaseDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                salesList.clear();
                for (DataSnapshot postSnapshot: snapshot.getChildren()) {

                    if (postSnapshot != null) {

                       /* for (DataSnapshot nestedSnapshot : postSnapshot.getChildren()) {
                            PurchaseCart purchasedItems = nestedSnapshot.getValue(PurchaseCart.class);
                            purchaseCart.add(purchasedItems);




                        }*/

                        Sales sale=postSnapshot.getValue(Sales.class);

                        salesList.add(sale);
                    }


                }


                Collections.sort(salesList, new Comparator<Sales>() {
                    @Override
                    public int compare(Sales lhs, Sales rhs) {
                        return (int) -(lhs.getSalesDate() - (rhs.getSalesDate()));
                    }
                });
                adapter = new SalesListAdapter (SalesActivity.this, salesList);
                rvSales.setAdapter(adapter);
                swipeRefreshLayout.setRefreshing(false);



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getMessage());
            }
        });




    }






    public void openAddPurchaseActivity(View v){
        ActivityOptions options =
                ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_right, R.anim.slide_left);

        Intent intent = new Intent(mContext, AddSalesActivity.class);
        startActivity(intent, options.toBundle());


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}
