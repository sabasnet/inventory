package com.deltatechnepal.deltaezinventory.main;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.deltatechnepal.deltaezinventory.R;
import com.deltatechnepal.deltaezinventory.helper.ConnectionChecker;
import com.deltatechnepal.deltaezinventory.helper.CustomToast;
import com.deltatechnepal.deltaezinventory.helper.MFunction;
import com.deltatechnepal.deltaezinventory.helper.Pathfinder;
import com.deltatechnepal.deltaezinventory.helper.SharedPreManager;
import com.deltatechnepal.deltaezinventory.model.Item;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;


public class ItemAddActivity extends AppCompatActivity {
    private static final String TAG = ItemAddActivity.class.getSimpleName();
    private int PICK_PHOTO_FROM_CAMERA=10,PICK_PHOTO_FROM_GALLERY=11,PICK_PHOTO_FROM_GALLERY_KITKAT=12;
    private Context mContext;
    ConstraintLayout mainLayout;
    EditText itemName,itemCode,itemQuantity,etOpeningQuantityVal;
    TextView tvProgress,tvProgressValue;
    ImageButton itemImage;
    private DatabaseReference mDatabaseRef;
    private FirebaseDatabase mFirebaseDatabase;
    private ValueEventListener addListener;
    String name,code,number,realPath,downloadUri="Empty";
    int quantity,openingItemQuantityVal;
    boolean imageSelected=false,success=false;
    ProgressBar progressBar;
    Uri originalUri = null;
    Uri tempUri;
    CustomToast cToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        //setting toolbar and actionbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        mContext = this;
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);


        cToast=new CustomToast();

        //Creating objects of UI components
        mainLayout=findViewById(R.id.main_layout);
        itemName=findViewById(R.id.item_name);
        itemCode=findViewById(R.id.item_code);
        itemQuantity=findViewById(R.id.item_quantity);
        etOpeningQuantityVal=findViewById(R.id.etOpeningItemQuantityVal);
        itemImage=findViewById(R.id.item_image);
        tvProgress=findViewById(R.id.tvProgress);
        tvProgressValue=findViewById(R.id.tvProgressValue);

        progressBar = findViewById(R.id.progressBar);



        Intent mIntent = getIntent();
        String addName = mIntent.getStringExtra("add_item");

        if(!TextUtils.isEmpty(addName)){
            itemName.setText(addName);
        }

        itemQuantity.setText("0");
        etOpeningQuantityVal.setText("0");


    }


    public void pickImage(View v) {
      ImageSourceSelector();
    }

    public void ImageSourceSelector() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_image_source, null);
        dialogBuilder.setView(dialogView);
        Button btnCamera=dialogView.findViewById(R.id.btn_camera);
        Button btnGallery=dialogView.findViewById(R.id.btn_gallery);


        dialogBuilder.setMessage(null);


        final AlertDialog b = dialogBuilder.create();
        b.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        /*b.getWindow().setBackgroundDrawableResource(R.drawable.rounded_white_background);*/
        b.show();

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();

                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, PICK_PHOTO_FROM_CAMERA);

            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();

                if (Build.VERSION.SDK_INT <19){
                    Intent intent = new Intent();
                    intent.setType("image/jpeg");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(intent,PICK_PHOTO_FROM_GALLERY);
                } else {
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("image/jpeg");
                    startActivityForResult(intent, PICK_PHOTO_FROM_GALLERY_KITKAT);
                }
            }
        });






    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode != Activity.RESULT_OK) return;
        if (null == data) return;

        if (requestCode == PICK_PHOTO_FROM_GALLERY) {
            originalUri = data.getData();
            realPath= Pathfinder.getPath(mContext,originalUri);

            Glide.with(this)
                    .load(realPath)
                    .into(itemImage);
            imageSelected=true;

        } else if (requestCode == PICK_PHOTO_FROM_GALLERY_KITKAT) {
            originalUri = data.getData();
            final int takeFlags = data.getFlags()
                    & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            // Check for the freshest data.
            getContentResolver().takePersistableUriPermission(originalUri, takeFlags);
            realPath= Pathfinder.getPath(mContext,originalUri);

            Glide.with(this)
                    .load(realPath)
                    .into(itemImage);
            imageSelected=true;


        }
        else if(requestCode == PICK_PHOTO_FROM_CAMERA){

            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            itemImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Glide.with(this)
                    .load(imageBitmap)
                    .into(itemImage);

           tempUri = getImageUri(getApplicationContext(), imageBitmap);

            realPath= Pathfinder.getPath(mContext,tempUri);
            imageSelected=true;


        }













    }




    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }




    public void submitItem(View v){
        number=SharedPreManager.getInstance(mContext).getUser().getMobileNumber();
        name=itemName.getText().toString();
        code=itemCode.getText().toString();




        if(TextUtils.isEmpty(name) || TextUtils.isEmpty(code) || TextUtils.isEmpty(itemQuantity.getText().toString())){
            if(TextUtils.isEmpty(name))
                itemName.setError("Please Enter Product Name");
            if(TextUtils.isEmpty(code))
                itemCode.setError("Please Enter Product Code");
            if(TextUtils.isEmpty(itemQuantity.getText().toString()))
                itemQuantity.setError("Please Enter Product Quantity");
            return;
            }

        quantity=Integer.valueOf(itemQuantity.getText().toString());
        openingItemQuantityVal = Integer.valueOf(etOpeningQuantityVal.getText().toString());


        if(ConnectionChecker.getInstance(mContext).Check() && imageSelected) {
            imageUpload();
        }
        else{
            downloadUri="https://firebasestorage.googleapis.com/v0/b/deltaez-inventory.appspot.com/o/itemImage%2Fcardboard-box.png?alt=media&token=a875aa2e-0a0e-4ddb-afbd-221ca3a85105";
            sendData();
        }


    }




    public void imageUpload(){
        // Get the data from an ImageView as bytes
        itemImage.setDrawingCacheEnabled(true);
        itemImage.buildDrawingCache();
        Bitmap bitmap = ((BitmapDrawable) itemImage.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        FirebaseStorage storage = FirebaseStorage.getInstance();

        StorageReference storageRef = storage.getReference();
        final StorageReference itemsImageRef = storageRef.child("itemImage/"+ name.replace(" ", ""));
        UploadTask uploadTask = itemsImageRef.putBytes(data);
        progressBar.setVisibility(View.VISIBLE);
        tvProgress.setVisibility(View.VISIBLE);
        tvProgressValue.setVisibility(View.VISIBLE);




        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                progressBar.setProgress((int) progress);
                tvProgressValue.setText(String.valueOf(progress)+"%");

                }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                // ...





            }
        });




        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return itemsImageRef.getDownloadUrl();
            }}).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    downloadUri = task.getResult().toString();
                    sendData();
                } else {
                    // Handle failures
                    // ...
                }
            }
        });




    }


public void sendData(){

        //Firebase
    mFirebaseDatabase = FirebaseDatabase.getInstance();

    // get reference to 'users' node
    mDatabaseRef = mFirebaseDatabase.getReference
            (number+"/Book/Item/").child(code);
    Item item = new Item(name, code, quantity,openingItemQuantityVal,downloadUri);
    item.createdAt = MFunction.getUnixTimestamp();
    mDatabaseRef.setValue(item);
    addItemChangeListener();

}




    /**
     * User data change listener
     */
    private void addItemChangeListener() {
        // User data change listener
        addListener= mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Item user = dataSnapshot.getValue(Item.class);

                // Check for null
                if (user == null) {
                    Log.e(TAG, "User data is null!");
                    return;
                }

                //Hide KeyBoard
                hideKeyboard(ItemAddActivity.this);
                cToast.showToast(ItemAddActivity.this,
                        getResources().getDrawable(R.drawable.ic_done),
                        "Successfully Added "+name);
                imageSelected=false;
                itemName.getText().clear();
                itemCode.getText().clear();
                itemQuantity.getText().clear();
                itemImage.setImageDrawable(null);
                itemImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_camera));
                itemImage.setScaleType(ImageView.ScaleType.CENTER);
                progressBar.setVisibility(View.GONE);
                tvProgress.setVisibility(View.GONE);
                tvProgressValue.setVisibility(View.GONE);

                mDatabaseRef.removeEventListener(addListener);
                finish();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });


    }













    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }


}
