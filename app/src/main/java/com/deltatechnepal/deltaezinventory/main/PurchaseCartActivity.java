package com.deltatechnepal.deltaezinventory.main;

import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.deltatechnepal.deltaezinventory.R;
import com.deltatechnepal.deltaezinventory.adapter.ItemsAdapter;
import com.deltatechnepal.deltaezinventory.adapter.PurchaseCartAdapter;
import com.deltatechnepal.deltaezinventory.helper.SharedPreManager;
import com.deltatechnepal.deltaezinventory.model.Item;
import com.deltatechnepal.deltaezinventory.model.Purchase;
import com.deltatechnepal.deltaezinventory.model.PurchaseCart;
import com.deltatechnepal.deltaezinventory.model.pCart;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class PurchaseCartActivity extends AppCompatActivity {
    private static final String TAG = PurchaseCartActivity.class.getSimpleName();
    private Context mContext;
    Button btnAddItem;
    TextView tvNoMatch,tvTotalPrice,tvTotalItems;
    LinearLayoutManager linearLayoutManager;
    RecyclerView rvItems,rvCart;
    String tempItemName ,number,rootKey,key;
    private ItemsAdapter adapter;
    private PurchaseCartAdapter cartAdapter;
    private List<pCart> itemCart = new ArrayList<>();
    private List<Item> itemList = new ArrayList<>();
    private List<PurchaseCart> purchaseList=new ArrayList<>();
    private List<Item> itemListTemp = new ArrayList<>();
    private DatabaseReference mDatabaseRef,mDatabaseRef1;
    private FirebaseDatabase mFirebaseDatabase;
    String cName,cImageUrl,cCode;
    boolean added=false;
    int cPrice,cQuantity,sQuantity,totalPrice,totalItems,counter=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_cart);
        //setting toolbar and actionbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        mContext = this;
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);

/*
        tvTotalPrice=findViewById(R.id.tvTotalBillValue);
        tvTotalItems=findViewById(R.id.tvTotalItemsValue);*/

        rvCart = findViewById(R.id.rvPurchaseCart);
        linearLayoutManager =
                new LinearLayoutManager(mContext);
        rvCart.setLayoutManager(linearLayoutManager);
        rvCart.setHasFixedSize(true);
        rvCart.setNestedScrollingEnabled(true);




    }



    private String getFormatedAmount(int amount){
        return NumberFormat.getNumberInstance(Locale.US).format(amount);
    }



    public void fetchItems()
    {

        cartAdapter=new PurchaseCartAdapter(PurchaseCartActivity.this,itemCart,purchaseList);
        rvCart.setAdapter(cartAdapter);


    }






    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}
