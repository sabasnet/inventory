package com.deltatechnepal.deltaezinventory.main;

import android.animation.Animator;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.RelativeLayout;


import com.deltatechnepal.deltaezinventory.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class SplashActivity extends Activity {
    private RelativeLayout layoutMain;
    String formattedDate;
    String URL;
    Animator animator;
    Context context;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);

        }
        formattedDate= new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        System.out.println("Formatted time => " + formattedDate);
        setContentView(R.layout.activity_splash);
        layoutMain =  findViewById(R.id.layoutMain);
        layoutMain.post(new Runnable() {
            @Override
            public void run() {
                revealAnimation();
            }
        });


    }

    public  void revealAnimation() {


        View myView = findViewById(R.id.logoLayout);

        // get the center for the clipping circle
        int cx = (myView.getLeft()+ myView.getRight())/2;
        int cy = myView.getBottom();

        // get the final radius for the clipping circle

        float finalRadius = (float)  (Math.hypot(myView.getWidth(), myView.getHeight()));




        animator =
                io.codetail.animation.ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);



        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setDuration(1500);
        animator.start();
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {

             exitSplash();

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {



            }
        });

    }


    public void exitSplash()
    {

        Intent i = new Intent(getBaseContext(), LoginActivity.class);
        ActivityOptions options =
                ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_right, R.anim.slide_left);

        startActivity(i,options.toBundle());
        finish();
    }



}