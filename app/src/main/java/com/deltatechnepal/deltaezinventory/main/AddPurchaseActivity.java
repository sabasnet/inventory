package com.deltatechnepal.deltaezinventory.main;

import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.deltatechnepal.deltaezinventory.R;
import com.deltatechnepal.deltaezinventory.adapter.ItemsAdapter;
import com.deltatechnepal.deltaezinventory.adapter.PurchaseCartAdapter;
import com.deltatechnepal.deltaezinventory.adapter.SalesCartAdapter;
import com.deltatechnepal.deltaezinventory.helper.CustomToast;
import com.deltatechnepal.deltaezinventory.helper.DateConverter;
import com.deltatechnepal.deltaezinventory.helper.MFunction;
import com.deltatechnepal.deltaezinventory.helper.SharedPreManager;
import com.deltatechnepal.deltaezinventory.model.Item;
import com.deltatechnepal.deltaezinventory.model.Purchase;
import com.deltatechnepal.deltaezinventory.model.PurchaseCart;
import com.deltatechnepal.deltaezinventory.model.pCart;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class AddPurchaseActivity extends AppCompatActivity {
    private static final String TAG = AddPurchaseActivity.class.getSimpleName();
    private Context mContext;
    Button btnAddItem;
    TextView tvNoMatch,tvTotalPrice,tvTotalItems,tvEmpty;
    ImageView ivEmpty;
    LinearLayoutManager linearLayoutManager;
    RecyclerView rvItems,rvCart;
    String tempItemName ,number,rootKey,key;
    private ItemsAdapter adapter;
    private PurchaseCartAdapter cartAdapter;
    private List<pCart> itemCart = new ArrayList<>();
    private List<Item> itemList = new ArrayList<>();
    private List<PurchaseCart> purchaseList=new ArrayList<>();
    private List<Item> itemListTemp = new ArrayList<>();
    private DatabaseReference mDatabaseRef,mDatabaseRef1,mDatabaseRef2;
    private FirebaseDatabase mFirebaseDatabase;
    String cName,cImageUrl,cCode;
    boolean added=false;
    Query query;
    CustomToast cToast;
    int cPrice,cQuantity,sQuantity,totalPrice,totalItems,counter=0,j;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_purchase);
        //setting toolbar and actionbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        mContext = this;
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);

        cToast=new CustomToast();

        tvTotalPrice=findViewById(R.id.tvTotalBillValue);
        tvTotalItems=findViewById(R.id.tvTotalItemsValue);
        tvEmpty=findViewById(R.id.tvEmpty);

        ivEmpty=findViewById(R.id.ivEmpty);

        rvCart = findViewById(R.id.rvCart);
        linearLayoutManager =
                new LinearLayoutManager(mContext);
        rvCart.setLayoutManager(linearLayoutManager);
        rvCart.setHasFixedSize(true);
        rvCart.setNestedScrollingEnabled(true);
        cartAdapter=new PurchaseCartAdapter(AddPurchaseActivity.this,itemCart,purchaseList);
        rvCart.setAdapter(cartAdapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(rvCart);



        // Register to receive messages.
        // We are registering an observer (mMessageReceiver) to receive Intents
        // with actions named "custom-message".
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("cart-message"));
    }


    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent

                cCode=intent.getStringExtra("item_code");
                cName = intent.getStringExtra("item_name");
                cPrice= intent.getIntExtra("item_price",0);
                cQuantity=intent.getIntExtra("item_quantity",0);
                sQuantity=intent.getIntExtra("stock_quantity",0);

                cImageUrl = intent.getStringExtra("item_image_url");
                pCart cart=new pCart(cCode,cName,cPrice,cQuantity,sQuantity,cImageUrl);
                itemCart.add(cart);
                PurchaseCart purchase=new PurchaseCart(cCode,cName,cPrice,cQuantity,cImageUrl);
                purchaseList.add(purchase);

              /*  cartAdapter.notifyDataSetChanged();*/


                totalItems+=cQuantity;
                totalPrice=totalPrice+(cPrice*cQuantity);
                cToast.showToast(AddPurchaseActivity.this,
                        getResources().getDrawable(R.drawable.ic_done),cName+" Added to Cart");




            tvTotalPrice.setText("Rs. "+getFormatedAmount(totalPrice));
            tvTotalItems.setText(String.valueOf(totalItems));


            if(purchaseList.size()==0)
            {
                rvCart.setVisibility(View.GONE);
                tvEmpty.setVisibility(View.VISIBLE);
                ivEmpty.setVisibility(View.VISIBLE);
            }
            else{
                rvCart.setVisibility(View.VISIBLE);
                tvEmpty.setVisibility(View.GONE);
                ivEmpty.setVisibility(View.GONE);
            }


        }
    };

    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0,  ItemTouchHelper.RIGHT) {

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                    RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                    int actionState, boolean isCurrentlyActive) {

            final View foregroundView = ((PurchaseCartAdapter.ViewHolder) viewHolder).viewForeground;

            getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX / 2, dY,
                    actionState, isCurrentlyActive);


        }


        public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

            final View foregroundView = ((PurchaseCartAdapter.ViewHolder) viewHolder).viewForeground;
            getDefaultUIUtil().clearView(foregroundView);

        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {


            final View foregroundView = ((PurchaseCartAdapter.ViewHolder) viewHolder).viewForeground;

            getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX / 2, dY,
                    actionState, isCurrentlyActive);


        }



     /*   @Override
        public int convertToAbsoluteDirection(int flags, int layoutDirection) {
            return super.convertToAbsoluteDirection(flags, layoutDirection);
        }*/


        @Override
        public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
            //Remove swiped item from list and notify the RecyclerView

            final int position = viewHolder.getAdapterPosition();
                new AlertDialog.Builder(mContext)
                        .setTitle(getString(R.string.app_name))
                        .setMessage("Remove from cart ?")
                        .setIcon(R.drawable.ic_info)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                          pCart cart=itemCart.get(position);
                                cCode=cart.getItemCode();
                                cName =cart.getItemName();
                                cPrice= cart.getItemPrice();
                                cQuantity=cart.getItemQuantity();
                                sQuantity=cart.getStockQuantity();
                                cImageUrl=cart.getItemImageUrl();

                                Item item=new Item(cName,cCode,sQuantity,0,cImageUrl);
                                // loop through all elements
                                for (int i = 0; i < itemListTemp.size(); i++) {
                                    // if the element you are looking at is smaller than x,
                                    // go to the next element
                                    if (itemListTemp.get(i).itemQuantity > sQuantity) {
                                        itemListTemp.add(i, item);
                                        added=true;
                                        break;
                                    }




                                }


                                //Logic to add at last
                                if(!added) {
                                    itemListTemp.add(item);
                                    added = false;
                                }

                                added=false;

                                totalItems=totalItems-cQuantity;
                                totalPrice=totalPrice-(cPrice*cQuantity);


                                tvTotalPrice.setText("Rs. "+getFormatedAmount(totalPrice));
                                tvTotalItems.setText(String.valueOf(totalItems));

                                purchaseList.remove(position);
                                itemCart.remove(position);
                                cartAdapter.notifyItemRemoved(position);
                                if(purchaseList.size()==0)
                                {
                                    rvCart.setVisibility(View.GONE);
                                    tvEmpty.setVisibility(View.VISIBLE);
                                    ivEmpty.setVisibility(View.VISIBLE);
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {


                                cartAdapter.notifyItemChanged(position);
                            }
                        }).show();

        }
    };



    private String getFormatedAmount(int amount){
        return NumberFormat.getNumberInstance(Locale.US).format(amount);
    }



    public void openBottomSheet(View v) {


        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_item_list, null);

        BottomSheetDialog dialog = new BottomSheetDialog(this,R.style.BaseBottomSheetDialog);
        dialog.setContentView(view);
        dialog.show();


        btnAddItem=dialog.findViewById(R.id.btnAddItem);
        tvNoMatch=dialog.findViewById(R.id.tvNoMatchFound);


        //SettingUp SearchView
        CardView cv=dialog.findViewById(R.id.searchHolder);
        final SearchView searchView = cv.findViewById(R.id.search);
        searchView.setSubmitButtonEnabled(true);
        searchView.setIconifiedByDefault(false);
        searchView.setQueryHint("Search Item....");
        ImageView b = searchView.findViewById(android.support.v7.appcompat.R.id.search_go_btn);
        b.setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

        // Get the search close button image view
        ImageView closeButton =searchView.findViewById(R.id.search_close_btn);

        // Set on click listener
        closeButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Find EditText view
                EditText et = searchView.findViewById(R.id.search_src_text);

                //Clear the text from EditText view
                et.setText("");
                tvNoMatch.setVisibility(View.GONE);
                btnAddItem.setVisibility(View.GONE);
            }
        });


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                tempItemName=query;
                if(adapter.getItemCount()==0){
                    tvNoMatch.setVisibility(View.VISIBLE);
                    btnAddItem.setVisibility(View.VISIBLE);
                    btnAddItem.setText("Add "+query);
                }
                else{
                    tvNoMatch.setVisibility(View.GONE);
                    btnAddItem.setVisibility(View.GONE);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });


        //setting Recyclerview

        rvItems =  dialog.findViewById(R.id.rvItems);
        linearLayoutManager =
                new LinearLayoutManager(mContext);
        rvItems.setLayoutManager(linearLayoutManager);
        rvItems.setHasFixedSize(true);
        rvItems.setNestedScrollingEnabled(true);


        fetchItems();


    }

    public void fetchItems()
    {


        if(itemListTemp.size()==0) {


            number = SharedPreManager.getInstance(mContext).getUser().getMobileNumber();
            //Firebase
            mFirebaseDatabase = FirebaseDatabase.getInstance();

            // get reference to 'users' node
            mDatabaseRef = mFirebaseDatabase.getReference
                    (number + "/Book/Item/");


            mDatabaseRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    itemList.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        Item item = postSnapshot.getValue(Item.class);
                        itemList.add(item);

                        // here you can access to name property like university.name

                    }

                /*    if (!TextUtils.isEmpty(removeName)) {
                        itemList.remove(itemList.contains(removeName));
                    }
*/

                    Collections.sort(itemList, new Comparator<Item>() {
                        @Override
                        public int compare(Item lhs, Item rhs) {
                            return -(lhs.getItemQuantity() - (rhs.getItemQuantity()));
                        }
                    });

                    Collections.reverse(itemList);


                    adapter = new ItemsAdapter(AddPurchaseActivity.this, itemList, 2);
                    rvItems.setAdapter(adapter);

                    itemListTemp=itemList;


                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    System.out.println("The read failed: " + databaseError.getMessage());
                }
            });
        }
       else{

            adapter = new ItemsAdapter(AddPurchaseActivity.this, itemListTemp, 2);
            rvItems.setAdapter(adapter);


            }



    }


    public void openAddItemActivity(View v){

        ActivityOptions options =
                ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_right, R.anim.slide_left);

        Intent intent = new Intent(mContext, ItemAddActivity.class);
        intent.putExtra("add_item",tempItemName);
        startActivity(intent, options.toBundle());

        }




        public void savePurchaseCart(View v){

            if(itemCart.size()!=0) {

                // get reference to 'users' node
                mDatabaseRef1 = mFirebaseDatabase.getReference
                        (number + "/Book/Purchase/");
                rootKey = mDatabaseRef1.push().getKey();


                mDatabaseRef2 = mFirebaseDatabase.getReference
                        (number + "/Book/Purchase/").child(rootKey);

                Purchase purchase = new Purchase(MFunction.getUnixTimestamp(), totalPrice, purchaseList);
                mDatabaseRef2.setValue(purchase);

                addItemChangeListener();
            }

            else{

                Toast.makeText(mContext,"No items Selected",Toast.LENGTH_SHORT).show();


            }



        }

    /**
     * User data change listener
     */
    private void addItemChangeListener() {
        // User data change listener
        mDatabaseRef2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Purchase purchase = dataSnapshot.getValue(Purchase.class);

                // Check for null
                if (purchase == null) {
                    Log.e(TAG, "Purchase data is null!");
                    return;
                }
               updateItemsStock();





            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
                cToast.showToast(AddPurchaseActivity.this,
                        getResources().getDrawable(R.drawable.ic_info_white),"Oops ! Something`s wrong with firebase");
            }
        });
    }



    public void updateItemsStock(){
        String code;
        for(int i=0;i<purchaseList.size();i++) {
            code=purchaseList.get(i).getItemCode();
            mDatabaseRef.child(code).child("itemQuantity").setValue(itemCart.get(i).stockQuantity+itemCart.get(i).itemQuantity);
        }
        cToast.showToast(AddPurchaseActivity.this,
                getResources().getDrawable(R.drawable.ic_done),"Purchase Cart Added and Stocks Updated Successfully");
        itemCart.clear();
        purchaseList.clear();
        cartAdapter.notifyDataSetChanged();
        tvTotalItems.setText("");
        tvTotalPrice.setText("");

        rvCart.setVisibility(View.GONE);
        tvEmpty.setVisibility(View.VISIBLE);
        ivEmpty.setVisibility(View.VISIBLE);

        finish();


    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }
}
