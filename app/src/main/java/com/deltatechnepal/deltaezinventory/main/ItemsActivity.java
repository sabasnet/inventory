package com.deltatechnepal.deltaezinventory.main;

import android.Manifest;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.deltatechnepal.deltaezinventory.R;
import com.deltatechnepal.deltaezinventory.adapter.ItemsAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.deltatechnepal.deltaezinventory.adapter.SalesListAdapter;
import com.deltatechnepal.deltaezinventory.helper.CustomToast;
import com.deltatechnepal.deltaezinventory.helper.InputFilterMinMax;
import com.deltatechnepal.deltaezinventory.helper.SharedPreManager;
import com.deltatechnepal.deltaezinventory.model.Item;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


public class ItemsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private Context mContext;
    private String TAG = ItemsActivity.class.getSimpleName();
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvItems;
    private LinearLayoutManager linearLayoutManager;
    private ItemsAdapter adapter;
    private List<Item> itemList = new ArrayList<>();
    private DatabaseReference mFirebaseDatabase,mDatabaseRef;
    private FirebaseDatabase mFirebaseInstance;
    private ValueEventListener mListener;
    String number,updatedName,updatesCode,updatedUrl;
    private int sort,updatedStock;
    FloatingActionButton fab;
    AlertDialog b;
    private ValueEventListener addListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);
        //setting toolbar and actionbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        mContext = this;
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);


        fab = findViewById(R.id.fabAdd);
        //setting Swipe to refresh and Recyclerview

        rvItems = findViewById(R.id.rvItems);
        linearLayoutManager =
                new LinearLayoutManager(mContext);
        rvItems.setLayoutManager(linearLayoutManager);
        rvItems.setHasFixedSize(true);
        rvItems.setNestedScrollingEnabled(true);
        final Animation animationLeft = AnimationUtils.loadAnimation(mContext,
                R.anim.s_left);
        animationLeft.setDuration(500);
        final Animation animationRight = AnimationUtils.loadAnimation(mContext,
                R.anim.s_right);
        animationRight.setDuration(500);


        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(rvItems);

        rvItems.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {


                if (linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                    //this is the top of the RecyclerView
                    fab.setVisibility(View.VISIBLE);
                    fab.startAnimation(animationLeft);
                } else {
                    if (fab.getVisibility() == View.VISIBLE) {
                        fab.setVisibility(View.GONE);
                        fab.startAnimation(animationRight);
                    }
                }


            }
        });


        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setRefreshing(true);
        fetchItems();


        swipeRefreshLayout.setOnRefreshListener(this);


        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {


            }
        });


    }

    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                    RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                    int actionState, boolean isCurrentlyActive) {

            final View foregroundView = ((ItemsAdapter.ViewHolder) viewHolder).viewForeground;

            getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX / 2, dY,
                    actionState, isCurrentlyActive);


        }


        public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

            final View foregroundView = ((ItemsAdapter.ViewHolder) viewHolder).viewForeground;
            getDefaultUIUtil().clearView(foregroundView);

        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {


            final View foregroundView = ((ItemsAdapter.ViewHolder) viewHolder).viewForeground;

            getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX / 2, dY,
                    actionState, isCurrentlyActive);


        }



     /*   @Override
        public int convertToAbsoluteDirection(int flags, int layoutDirection) {
            return super.convertToAbsoluteDirection(flags, layoutDirection);
        }*/


        @Override
        public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
            //Remove swiped item from list and notify the RecyclerView

            final int position = viewHolder.getAdapterPosition();

            if(direction==ItemTouchHelper.RIGHT) {
                new AlertDialog.Builder(mContext)
                        .setTitle(getString(R.string.app_name))
                        .setMessage("Delete this item ?")
                        .setIcon(R.drawable.ic_info)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                deleteItem(position);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                adapter.notifyItemChanged(viewHolder.getAdapterPosition());
                            }
                        }).show();
            }
            else{

                editItem(position);


            }
        }
    };


    public void editItem(final int position){



            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            final LayoutInflater inflater = this.getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.dialog_item_edit, null);
            dialogBuilder.setView(dialogView);




            final EditText itemName = dialogView.findViewById(R.id.item_name);
            final EditText itemCode =  dialogView.findViewById(R.id.item_code);

            final Item item=itemList.get(position);

            itemName.setText(item.itemName);
            itemCode.setText(item.itemCode);



            dialogBuilder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //do something with edt.getText().toString();


                    updatedName=itemName.getText().toString();
                    updatesCode=itemCode.getText().toString();
                    updatedStock=item.itemQuantity;
                    updatedUrl=item.itemImageUrl;

                    final Query update = mFirebaseDatabase.orderByChild("itemCode").equalTo(item.itemCode);

                    update.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange (DataSnapshot dataSnapshot){
                            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                postSnapshot.getRef().removeValue();

                                mDatabaseRef = mFirebaseDatabase.child(itemCode.getText().toString());
                                Item item = new Item(updatedName,updatesCode,updatedStock,0,updatedUrl);
                                mDatabaseRef.setValue(item);
                                addItemChangeListener();
                                adapter.notifyItemChanged(position);
                            }
                        }

                        @Override
                        public void onCancelled (DatabaseError databaseError){
                            Log.e(TAG, "onCancelled", databaseError.toException());
                        }
                    });











                }
            });
            dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                  adapter.notifyItemChanged(position);
                }
            });


            b = dialogBuilder.create();
            b.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
            b.show();
            b.setTitle("Edit");
            dialogBuilder.setMessage(null);






        }



    /**
     * User data change listener
     */
    private void addItemChangeListener() {
        // User data change listener
        addListener= mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Item item = dataSnapshot.getValue(Item.class);

                // Check for null
                if (item == null) {
                    Log.e(TAG, "User data is null!");
                    return;
                }

                CustomToast cToast= new CustomToast();
                cToast.showToast(ItemsActivity.this,
                        getResources().getDrawable(R.drawable.ic_done),
                        "Successfully Updated "+item.itemName);
                mDatabaseRef.removeEventListener(addListener);

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });


    }















    public void deleteItem(final int position) {

        Item item=itemList.get(position);

    final Query delete = mFirebaseDatabase.orderByChild("itemCode").equalTo(item.itemCode);

        delete.addListenerForSingleValueEvent(new ValueEventListener() {
        @Override
        public void onDataChange (DataSnapshot dataSnapshot){
            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                postSnapshot.getRef().removeValue();
                itemList.remove(position);
                adapter.notifyItemRemoved(position);
            }
        }

        @Override
        public void onCancelled (DatabaseError databaseError){
            Log.e(TAG, "onCancelled", databaseError.toException());
        }
    });
}


    /**
     * This method is called when swipe refresh is pulled down
     */
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        fetchItems();
        swipeRefreshLayout.setRefreshing(false);

    }



    public void fetchItems()
    {
        number= SharedPreManager.getInstance(mContext).getUser().getMobileNumber();
        //Firebase
        mFirebaseInstance = FirebaseDatabase.getInstance();

        // get reference to 'users' node
        mFirebaseDatabase = mFirebaseInstance.getReference
                (number+"/Book/Item/");


       mListener= mFirebaseDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                itemList.clear();
                for (DataSnapshot postSnapshot: snapshot.getChildren()) {
                    Item item = postSnapshot.getValue(Item.class);
                    itemList.add(item);

                    // here you can access to name property like university.name

                }

                if(itemList !=null) {
                   sortList();


                   // Collections.reverse(itemList);



                    adapter = new ItemsAdapter(ItemsActivity.this, itemList,1);
                    rvItems.setAdapter(adapter);
                }



                swipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getMessage());
            }
        });



    }


    public void AddItem(View v){

        Intent intent = new Intent(mContext, ItemAddActivity.class);
        ActivityOptions options =
                ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_right, R.anim.slide_left);
        startActivity(intent, options.toBundle());



    }



    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });



        // Get the search close button image view
        ImageView closeButton =searchView.findViewById(R.id.search_close_btn);

        // Set on click listener
        closeButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Find EditText view
                EditText et = searchView.findViewById(R.id.search_src_text);

                //Clear the text from EditText view
                et.setText("");

            }
        });

        return super.onCreateOptionsMenu(menu);
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            case R.id.action_sort:
            {

                showSortDialog();


            }


            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }


    public void showSortDialog(){


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        final LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_sort, null);
        dialogBuilder.setView(dialogView);





        final RadioGroup radioGroup=dialogView.findViewById(R.id.sort_radio_group);
        final RadioButton rbName=radioGroup.findViewById(R.id.name);
        final RadioButton rbCode=radioGroup.findViewById(R.id.code);
        final RadioButton rbStock=radioGroup.findViewById(R.id.stock);
        final CheckBox cbReverse=dialogView.findViewById(R.id.cbReverse);

        int sortValue=SharedPreManager.getInstance(mContext).getSortValue();
        switch (sortValue)
        {
            case 1:
                rbName.setChecked(true);
                break;
            case 2:
                rbCode.setChecked(true);
                break;
            case 3:
                rbStock.setChecked(true);
                break;

        }
        if(SharedPreManager.getInstance(mContext).isSortReversed())
            cbReverse.setChecked(true);







        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();

                int position=radioGroup.getCheckedRadioButtonId();

                if(position == rbName.getId()) {
                   sort=1;

                } else if(position == rbCode.getId()) {
                    sort=2;
                } else if(position == rbStock.getId()){
                   sort=3;
                }


                SharedPreManager.getInstance(mContext).sortItemValue(sort);


                if(cbReverse.isChecked())
                    SharedPreManager.getInstance(mContext).sortOrder(true);
                else
                    SharedPreManager.getInstance(mContext).sortOrder(false);


                sortList();
                adapter.notifyDataSetChanged();


                }


        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog b = dialogBuilder.create();
        b.setTitle("Sort by:");
        b.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        b.show();






    }



    public void sortList(){

       int sortValue=SharedPreManager.getInstance(mContext).getSortValue();

        switch (sortValue){
            case 1:
                Collections.sort(itemList, new Comparator<Item>() {
                    @Override
                    public int compare(Item lhs, Item rhs) {
                        return lhs.getItemName().compareToIgnoreCase(rhs.getItemName());
                    }
                });
                break;
            case 2:
                Collections.sort(itemList, new Comparator<Item>() {
                    @Override
                    public int compare(Item lhs, Item rhs) {
                        return lhs.getItemCode().compareToIgnoreCase(rhs.getItemCode());
                    }
                });
                break;
            case 3:
                Collections.sort(itemList, new Comparator<Item>() {
                    @Override
                    public int compare(Item lhs, Item rhs) {
                        return (lhs.getItemQuantity() - (rhs.getItemQuantity()));
                    }
                });
                break;





        }

        if(SharedPreManager.getInstance(mContext).isSortReversed()){
            Collections.reverse(itemList);
        }





    }




    @Override
    public void onDestroy(){
        super.onDestroy();
        mFirebaseDatabase.removeEventListener(mListener);


    }
}
