package com.deltatechnepal.deltaezinventory.main;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.deltatechnepal.deltaezinventory.R;
import com.deltatechnepal.deltaezinventory.adapter.ItemsAdapter;
import com.deltatechnepal.deltaezinventory.adapter.PurchaseTableAdapter;
import com.deltatechnepal.deltaezinventory.helper.MFunction;
import com.deltatechnepal.deltaezinventory.helper.SharedPreManager;
import com.deltatechnepal.deltaezinventory.model.InventoryRow;
import com.deltatechnepal.deltaezinventory.model.Item;
import com.deltatechnepal.deltaezinventory.model.Purchase;
import com.deltatechnepal.deltaezinventory.model.PurchaseCart;
import com.deltatechnepal.deltaezinventory.model.Sales;
import com.deltatechnepal.deltaezinventory.model.SalesCart;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import de.codecrafters.tableview.SortableTableView;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;


public class ItemDetailActivity extends AppCompatActivity {
    String number;
    private Context mContext;
    private FirebaseDatabase mFirebaseInstance;
    private DatabaseReference mFirebaseDatabase;
    private ValueEventListener mListener;
    SortableTableView sortableTableView;

    Integer stockOpening = 0;
    Integer stockIn = 0;
    Integer stockOut = 0;
    Integer remainingStock = 0;

    final List<InventoryRow> saleRowList = new ArrayList<>();

    private static final String[] TABLE_HEADERS = { "Date", "Type", "Qty", "Value"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_item_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);

        Bundle bundle = getIntent().getExtras().getBundle("bundle");
        String strItem = bundle.getString("item");
        Item item = new Gson().fromJson(strItem, Item.class);
        remainingStock = item.itemQuantity;

        sortableTableView = (SortableTableView) findViewById(R.id.tableView);

        sortableTableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, TABLE_HEADERS));
        sortableTableView.setHeaderElevation(10);
        sortableTableView.setHeaderBackground(R.color.colorAccentLite);
        //sortableTableView.setColumnComparator(0,new CreatedDateComparator());
        fetchSales(item);
        fetchTableData(item);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }

    private static class CreatedDateComparator implements Comparator<Item> {
        @Override
        public int compare(Item item1, Item item2) {
            Long date1 = item1.getCreatedAt();
            Long date2 = item2.getCreatedAt();
            return date1.compareTo(date2);
        }
    }

    private void fetchTableData(final Item currentItem) {
        number= SharedPreManager.getInstance(mContext).getUser().getMobileNumber();
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference(number+"/Book/Purchase/");

        final List<InventoryRow> rowList = new ArrayList<>();


        mListener= mFirebaseDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                rowList.clear();

                InventoryRow row = new InventoryRow();
                    row.unixCreatedAt = currentItem.createdAt;
                    row.createdAt = MFunction.getDateTime(currentItem.createdAt);
                    row.type = "Opening";
                    row.quantity = String.valueOf(currentItem.openingQuantity);
                    stockOpening = currentItem.openingQuantity;
                    row.price = String.valueOf(currentItem.openingItemQuantityVal);
                rowList.add(row);
                for (DataSnapshot postSnapshot: snapshot.getChildren()) {
                    Purchase purchase = postSnapshot.getValue(Purchase.class);
                    String purchaseDate = MFunction.getDateTime(purchase.getPurchaseDate());
                    List<PurchaseCart> pcList = purchase.getPurchaseCart();
                    for (PurchaseCart purchaseCart:pcList) {
                        if(purchaseCart != null){
                            String tempItemCode = purchaseCart.getItemCode();
                            if(TextUtils.equals(currentItem.itemCode,tempItemCode)){
                                InventoryRow tempRow = new InventoryRow();
                                tempRow.unixCreatedAt = purchase.getPurchaseDate();
                                tempRow.createdAt = purchaseDate;
                                tempRow.type = "Purchase";
                                tempRow.quantity = String.valueOf(purchaseCart.itemQuantity);
                                tempRow.price = String.valueOf(purchaseCart.itemPrice);
                                stockIn += purchaseCart.itemQuantity;
                                rowList.add(tempRow);
                            }
                        }
                    }
                }

                stockOut = stockOpening + stockIn - currentItem.itemQuantity;

                List<InventoryRow> finalList = new ArrayList<>(rowList);
                finalList.addAll(saleRowList);
                Collections.sort(finalList,InventoryRow.comparatorDateAsc);


                sortableTableView.setDataAdapter(new PurchaseTableAdapter(mContext, finalList,sortableTableView));

                String averageValuePerUnit = calculateAverageValuePerUnit(rowList);
                String fifoValuePerUnit = calculateValuePerUnit(rowList,true);
                String lifoValuePerUnit = calculateValuePerUnit(rowList,false);

                TextView tvStockLevelValue = findViewById(R.id.tvStockLevelValue);
                tvStockLevelValue.setText(String.valueOf(currentItem.itemQuantity));

                TextView tvValuePerUnitValue = findViewById(R.id.tvValuePerUnitValue);
                tvValuePerUnitValue.setText(String.valueOf(fifoValuePerUnit));

                TextView tvValuePerUnitLifoValue = findViewById(R.id.tvValuePerUnitLifoValue);
                tvValuePerUnitLifoValue.setText(String.valueOf(lifoValuePerUnit));

                TextView tvTotalValueVal = findViewById(R.id.tvTotalValueVal);
                tvTotalValueVal.setText(String.valueOf(averageValuePerUnit));



                TextView tvDetail = findViewById(R.id.tvDetail);
                String detailText = "";
                detailText +="Sold:"+stockOut;
                //detailText +="\n Average Value Per Unit:Rs "+averageValuePerUnit;
                //detailText +="\n Value per unit(FIFO sold):Rs "+fifoValuePerUnit;
                //detailText +="\n Value per unit(LIFO sold):Rs "+lifoValuePerUnit;
                tvDetail.setText(detailText);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getMessage());
            }
        });
    }

    private void fetchSales(final Item currentItem) {

        String number= SharedPreManager.getInstance(mContext).getUser().getMobileNumber();
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference(number+"/Book/Sales/");

        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                saleRowList.clear();
                for (DataSnapshot postSnapshot: snapshot.getChildren()) {
                    Sales sale = postSnapshot.getValue(Sales.class);
                    String saleDate = MFunction.getDateTime(sale.getSalesDate());
                    List<SalesCart> scList = sale.getSalesCart();
                    if(scList == null) return;
                    for (SalesCart salesCart:scList) {
                        if(salesCart != null){
                            String tempItemCode = salesCart.getItemCode();
                            if(TextUtils.equals(currentItem.itemCode,tempItemCode)){
                                InventoryRow tempRow = new InventoryRow();
                                tempRow.unixCreatedAt = sale.getSalesDate();
                                tempRow.createdAt = saleDate;
                                tempRow.type = "Sale";
                                tempRow.quantity = String.valueOf(salesCart.itemQuantity);
                                tempRow.price = "--";
                                saleRowList.add(tempRow);
                                Log.i("tempRow","tempRow Added in saleRowList");
                            }
                        }
                    }
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getMessage());
            }
        });
    }

    private String calculateAverageValuePerUnit(List<InventoryRow> tempList){
        if(tempList.isEmpty()) return "";
        int qvSummation = 0;
        int qSummation = 0;
        for (InventoryRow row:tempList) {
            int q = Integer.parseInt(row.quantity);
            int v = Integer.parseInt(row.price);
            qvSummation = qvSummation + (q*v);
            qSummation = qSummation + q;
        }

        Double avgValPerUnit = (double) qvSummation/qSummation;

        return String.valueOf(MFunction.roundDown2(avgValPerUnit));

    }

    private String calculateValuePerUnit(List<InventoryRow> rowList, Boolean isFifo){
        List<InventoryRow> tempList  = new ArrayList<>(rowList);
        if(isFifo){
            Collections.sort(tempList,InventoryRow.comparatorDateDesc);

        } else {

        }

        if(tempList.isEmpty()) return "";
        int qvSummation = 0;
        int qSummation = 0;
        int rQuantity = remainingStock;
        for (InventoryRow row:tempList) {
            if(rQuantity <=0) break;

            int v = Integer.parseInt(row.price);
            int q = Integer.parseInt(row.quantity);

            if(rQuantity < q){
                q = rQuantity;
            }
            qvSummation = qvSummation + (q*v);
            qSummation = qSummation + q;
            rQuantity = rQuantity - q;

        }

        Double valPerUnit = (double) qvSummation/qSummation;
        return String.valueOf(MFunction.roundDown2(valPerUnit));

    }

}