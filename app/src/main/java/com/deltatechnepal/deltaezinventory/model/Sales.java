package com.deltatechnepal.deltaezinventory.model;

import java.util.List;

public class Sales {
        String salesCode;
        long salesDate;
        List<SalesCart> salesCart;

        public Sales(){

        }



        public Sales(String salesCode,long salesDate, List<SalesCart> salesCart) {
            /*  this.itemImage=itemImage;*/
            this.salesCode=salesCode;
            this.salesDate= salesDate;
            this.salesCart=salesCart;


        }

    public String getSalesCode() {
        return salesCode;
    }

    public long getSalesDate() {
        return salesDate;
    }

    public List<SalesCart> getSalesCart() { return salesCart; }


}