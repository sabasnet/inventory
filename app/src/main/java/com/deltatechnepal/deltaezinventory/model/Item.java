package com.deltatechnepal.deltaezinventory.model;


import com.orm.SugarRecord;

import java.util.Comparator;

public class Item {
   /* public String itemImage;*/
    public String itemName;
    public String itemCode;
    public int itemQuantity;
    public int openingQuantity;
    public int openingItemQuantityVal;
    public String itemImageUrl;
    public long createdAt;


    public Item(){

    }


    public Item(String itemName, String itemCode, int itemQuantity,int openingItemQuantityVal,String itemImageUrl) {
      /*  this.itemImage=itemImage;*/
        this.itemName=itemName;
        this.itemCode=itemCode;
        this.itemQuantity=itemQuantity;
        this.openingQuantity = itemQuantity;
        this.openingItemQuantityVal=openingItemQuantityVal;
        this.itemImageUrl=itemImageUrl;


    }

    public String getItemName() {
        return itemName;
    }

    public String getItemCode() {
        return itemCode;
    }

    public int getItemQuantity() {
        return itemQuantity;
    }

    public int getOpeningItemQuantityVal() {
        return openingItemQuantityVal;
    }

    public String getItemImageUrl() {
        return itemImageUrl;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    /*Comparator for sorting  by createdAt*/
    /*public static Comparator<Item> comparatorDateDesc = new Comparator<Item>() {

        public int compare(Item c1, Item c2) {
            Long name1 = c1.createdAt;
            Long name2 = c2.createdAt;

            //ascending order
            //return name1.compareTo(name2);

            //descending order
            return name2.compareTo(name1);
        }
    };*/


}
