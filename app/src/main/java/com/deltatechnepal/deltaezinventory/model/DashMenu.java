package com.deltatechnepal.deltaezinventory.model;


public class DashMenu {
    public String title;
    public String activity;
    public int drawable;


    public DashMenu() {
    }

    public DashMenu(String title, String activity ,int drawable) {
        this.title = title;
        this.activity = activity;
        this.drawable = drawable;
    }
}
