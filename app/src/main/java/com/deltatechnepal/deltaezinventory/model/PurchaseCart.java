package com.deltatechnepal.deltaezinventory.model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class PurchaseCart {
        public String itemName;
        private String itemCode;
        public int itemPrice;
        public int itemQuantity;
        public String itemImageUrl;


        public PurchaseCart(){

        }



        public PurchaseCart(String itemCode, String itemName, int itemPrice, int itemQuantity, String itemImageUrl) {
            /*  this.itemImage=itemImage;*/
            this.itemCode=itemCode;
            this.itemName=itemName;
            this.itemPrice=itemPrice;
            this.itemQuantity=itemQuantity;
            this.itemImageUrl=itemImageUrl;


        }

    public String getItemCode() { return itemCode; }

    public String getItemName() {
            return itemName;
        }

        public int getItemPrice() {
            return itemPrice;
        }

        public int getItemQuantity() {
            return itemQuantity;
        }

    public String getItemImageUrl() {
            return itemImageUrl;
        }




    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("itemCode", itemCode);
        result.put("itemName",itemName);
        result.put("itemPrice",itemPrice);
        result.put("itemQuantity",itemQuantity);
        result.put("itemImageUrl",itemImageUrl);

        return result;
    }
}