package com.deltatechnepal.deltaezinventory.model;

public class pCart   {
        public String itemName;
        private String itemCode;
        public int itemPrice;
        public int itemQuantity;
        public int stockQuantity;
        public String itemImageUrl;


        public pCart(){

        }



        public pCart(String itemCode,String itemName, int itemPrice, int itemQuantity,int stockQuantity,String itemImageUrl) {
            /*  this.itemImage=itemImage;*/
            this.itemCode=itemCode;
            this.itemName=itemName;
            this.itemPrice=itemPrice;
            this.itemQuantity=itemQuantity;
            this.stockQuantity=stockQuantity;
            this.itemImageUrl=itemImageUrl;


        }

    public String getItemCode() { return itemCode; }

    public String getItemName() {
            return itemName;
        }

        public int getItemPrice() {
            return itemPrice;
        }

        public int getItemQuantity() {
            return itemQuantity;
        }

    public int getStockQuantity() { return stockQuantity; }

    public String getItemImageUrl() {
            return itemImageUrl;
        }
}