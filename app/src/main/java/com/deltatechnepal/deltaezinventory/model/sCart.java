package com.deltatechnepal.deltaezinventory.model;

public class sCart {
        public String itemName;
        private String itemCode;
        public int itemQuantity;
        public int stockQuantity;
        public String itemImageUrl;


        public sCart(){

        }



        public sCart(String itemCode, String itemName, int itemQuantity, int stockQuantity, String itemImageUrl) {
            /*  this.itemImage=itemImage;*/
            this.itemCode=itemCode;
            this.itemName=itemName;
            this.itemQuantity=itemQuantity;
            this.stockQuantity=stockQuantity;
            this.itemImageUrl=itemImageUrl;


        }

    public String getItemCode() { return itemCode; }

    public String getItemName() {
            return itemName;
        }

        public int getItemQuantity() {
            return itemQuantity;
        }

    public int getStockQuantity() { return stockQuantity; }

    public String getItemImageUrl() {
            return itemImageUrl;
        }
}