package com.deltatechnepal.deltaezinventory.model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class SalesCart {
        public String itemName;
        public String itemCode;
    public int itemQuantity;
    public String itemPrice;
    public String itemImageUrl;


        public SalesCart(){

        }



        public SalesCart(String itemCode, String itemName, String itemPrice, int itemQuantity, String itemImageUrl) {
            /*  this.itemImage=itemImage;*/
            this.itemCode=itemCode;
            this.itemName=itemName;
            this.itemQuantity=itemQuantity;
            this.itemPrice = itemPrice;
            this.itemImageUrl=itemImageUrl;


        }

    public String getItemCode() { return itemCode; }

    public String getItemName() {
            return itemName;
        }


        public int getItemQuantity() {
            return itemQuantity;
        }

    public String getItemImageUrl() {
            return itemImageUrl;
        }




    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("itemCode", itemCode);
        result.put("itemName",itemName);
        result.put("itemQuantity",itemQuantity);
        result.put("itemImageUrl",itemImageUrl);

        return result;
    }
}