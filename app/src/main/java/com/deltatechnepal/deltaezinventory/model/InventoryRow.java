package com.deltatechnepal.deltaezinventory.model;

import java.util.Comparator;

public class InventoryRow {
        public Long unixCreatedAt;
        public String createdAt;
        public String type;
        public String quantity;
        public String price;

        public InventoryRow(){

        }


        public static Comparator<InventoryRow> comparatorDateDesc = new Comparator<InventoryRow>() {

                public int compare(InventoryRow c1, InventoryRow c2) {
                        Long name1 = c1.unixCreatedAt;
                        Long name2 = c2.unixCreatedAt;

                        //descending order
                        return name2.compareTo(name1);
                }
        };

        public static Comparator<InventoryRow> comparatorDateAsc = new Comparator<InventoryRow>() {

                public int compare(InventoryRow c1, InventoryRow c2) {
                        Long name1 = c1.unixCreatedAt;
                        Long name2 = c2.unixCreatedAt;

                        //descending order
                        return name1.compareTo(name2);
                }
        };
}