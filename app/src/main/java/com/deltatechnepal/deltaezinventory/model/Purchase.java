package com.deltatechnepal.deltaezinventory.model;

import com.deltatechnepal.deltaezinventory.helper.DateConverter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Purchase {
        long purchaseDate;
        long totalPrice;
        List<PurchaseCart> purchaseCart;

        public Purchase(){

        }



        public Purchase(long purchaseDate,long totalPrice,List<PurchaseCart> purchaseCart) {
            /*  this.itemImage=itemImage;*/
            this.purchaseDate= purchaseDate;
            this.totalPrice=totalPrice;
            this.purchaseCart=purchaseCart;


        }



        public long getTotalPrice(){return totalPrice; }

    public long getPurchaseDate() {
        return purchaseDate;
    }

    public List<PurchaseCart> getPurchaseCart() { return purchaseCart; }


}