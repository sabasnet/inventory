package com.deltatechnepal.deltaezinventory.model;


public class User {

    private String UserId;
    private String MobileNumber;

    public User(String UserId, String MobileNumber) {
        this.UserId = UserId;
        this.MobileNumber = MobileNumber;

    }

    public String getUserId() {
        return UserId;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }





}
