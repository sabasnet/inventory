package com.deltatechnepal.deltaezinventory.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.deltatechnepal.deltaezinventory.helper.MFunction;
import com.deltatechnepal.deltaezinventory.model.InventoryRow;
import com.deltatechnepal.deltaezinventory.model.PurchaseCart;

import java.util.List;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.LongPressAwareTableDataAdapter;

public class PurchaseTableAdapter extends LongPressAwareTableDataAdapter<InventoryRow> {

    public PurchaseTableAdapter(Context context, List<InventoryRow> data,final TableView<InventoryRow> tableView) {
        super(context, data,tableView);
    }

    @Override
    public View getLongPressCellView(int rowIndex, int columnIndex, ViewGroup parentView) {
        final InventoryRow inventoryRow = getRowData(rowIndex);
        View renderedView = null;

        switch (columnIndex) {
            case 1:
                //
                break;
            default:
                renderedView = getDefaultCellView(rowIndex, columnIndex, parentView);
        }

        return renderedView;
    }

    @Override
    public View getDefaultCellView(int rowIndex, int columnIndex, ViewGroup parentView) {
        InventoryRow inventoryRow = getRowData(rowIndex);
        View renderedView = null;

        switch (columnIndex) {
            case 0:
                renderedView = renderDate(inventoryRow);
                break;
            case 1:
                renderedView = renderType(inventoryRow);
                break;

            case 2:
                renderedView = renderQuantity(inventoryRow);
                break;

            case 3:
                renderedView = renderValue(inventoryRow);
                break;
        }

        return renderedView;

    }

    private View renderDate(final InventoryRow inventoryRow) {

        final TextView textView = new TextView(getContext());
        textView.setText(MFunction.getDateTime(inventoryRow.unixCreatedAt));
        textView.setPadding(20, 10, 20, 10);
        return textView;
    }

    private View renderType(final InventoryRow inventoryRow) {
        final TextView textView = new TextView(getContext());
        textView.setText(inventoryRow.type);
        textView.setPadding(20, 10, 20, 10);
        return textView;
    }

    private View renderQuantity(final InventoryRow inventoryRow) {
        final TextView textView = new TextView(getContext());
        textView.setText(inventoryRow.quantity);
        textView.setPadding(20, 10, 20, 10);
        return textView;
    }

    private View renderValue(final InventoryRow inventoryRow) {
        final TextView textView = new TextView(getContext());
        textView.setText(inventoryRow.price);
        textView.setPadding(20, 10, 20, 10);
        return textView;
    }


}
