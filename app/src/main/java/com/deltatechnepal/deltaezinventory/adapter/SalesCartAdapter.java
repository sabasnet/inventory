package com.deltatechnepal.deltaezinventory.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.deltatechnepal.deltaezinventory.R;
import com.deltatechnepal.deltaezinventory.model.SalesCart;
import com.deltatechnepal.deltaezinventory.model.sCart;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;


public class SalesCartAdapter extends RecyclerView.Adapter<SalesCartAdapter.ViewHolder>{
    private Activity activity;
    private List<sCart> itemList;
    private List<SalesCart> salesList;
    EditText pItemName,pItemPrice,pItemQuantity;
    int position;

    private View v;
    private SalesCartAdapter.ViewHolder mh;

    public SalesCartAdapter(Activity activity, List<sCart> itemList, List<SalesCart> salesList) {
        this.activity = activity;
        this.itemList = itemList;
        this.salesList=salesList;

        /* bgColors = activity.getApplicationContext().getResources().getStringArray(R.array.movie_serial_bg);*/
    }
    public class ViewHolder extends RecyclerView.ViewHolder  {

        protected TextView tvName,tvQuantity;
        public ImageView ivItem;
        protected ViewGroup transitionsContainer;
        public LinearLayout viewBackground;
        public ConstraintLayout viewForeground;



        public ViewHolder(View view) {

            super(view);
            viewBackground = view.findViewById(R.id.viewBackground);
            viewForeground = view.findViewById(R.id.viewForeground);
            this.transitionsContainer = view.findViewById(R.id.top_holder);
            this.tvName = view.findViewById(R.id.itemName);
            this.tvQuantity = view.findViewById(R.id.itemQuantity);
            this.ivItem = view.findViewById(R.id.itemImage);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    position=getAdapterPosition();
                    String name=itemList.get(position).getItemName();
                    String code=itemList.get(position).getItemCode();
                    int quantity=itemList.get(position).getStockQuantity();
                    String url=itemList.get(position).getItemImageUrl();



                }
            });


        }


    }

    @Override
    public SalesCartAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_add_sales_items,viewGroup, false);
        mh = new SalesCartAdapter.ViewHolder(v);

        return mh;
    }

    @Override
    public void onBindViewHolder(SalesCartAdapter.ViewHolder holder, int i) {
        sCart item = itemList.get(i);
        holder.tvName.setText(item.getItemName());
        holder.tvQuantity.setText(String.valueOf(item.getItemQuantity()));
        Glide.with(activity)
                .load(item.getItemImageUrl())
                .into(holder.ivItem);









    }

    @Override
    public int getItemCount() {
        return (itemList.size());
    }


    private String getFormatedAmount(int amount){
        return NumberFormat.getNumberInstance(Locale.US).format(amount);
    }











}