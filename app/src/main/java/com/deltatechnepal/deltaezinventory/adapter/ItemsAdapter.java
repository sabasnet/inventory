package com.deltatechnepal.deltaezinventory.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.deltatechnepal.deltaezinventory.R;

import java.util.ArrayList;
import java.util.List;

import com.deltatechnepal.deltaezinventory.helper.CustomToast;
import com.deltatechnepal.deltaezinventory.helper.InputFilterMinMax;
import com.deltatechnepal.deltaezinventory.main.ItemDetailActivity;
import com.deltatechnepal.deltaezinventory.main.LoginActivity;
import com.deltatechnepal.deltaezinventory.model.Item;
import com.google.gson.Gson;


public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ViewHolder> implements Filterable {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Item> itemList;
    private List<Item> itemListFiltered;
    EditText pItemName,pItemPrice,pItemQuantity;
    int mode,position;
    private int lastPosition = -1;
    AlertDialog b;
    String enteredQuantity;

    private View v;
    private ItemsAdapter.ViewHolder mh;


    public ItemsAdapter(Activity activity, List<Item> itemList,int mode) {
        this.activity = activity;
        this.mode=mode;
        this.itemList = itemList;
        this.itemListFiltered = itemList;
        /* bgColors = activity.getApplicationContext().getResources().getStringArray(R.array.movie_serial_bg);*/
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {

        protected TextView tvName,tvCode,tvQuantity;
        public ImageView ivItem;
        protected ViewGroup transitionsContainer;
        public LinearLayout viewBackground;
        public  ConstraintLayout viewForeground;




        public ViewHolder(View view) {

            super(view);
            viewBackground = view.findViewById(R.id.viewBackground);
            viewForeground = view.findViewById(R.id.viewForeground);
            this.transitionsContainer = view.findViewById(R.id.top_holder);
            this.tvName = view.findViewById(R.id.itemName);
            this.tvCode = view.findViewById(R.id.itemCode);
            this.tvQuantity = view.findViewById(R.id.itemQuantity);
            this.ivItem = view.findViewById(R.id.itemImage);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                  position=getAdapterPosition();
                    switch (mode){

                        case 2:
                            openAddPurchaseDialog(itemListFiltered.get(position).getItemName());
                            break;
                        case 3:
                            openAddSalesDialog(itemListFiltered.get(position).getItemName());
                            break;

                        default:{
                            Item item = itemList.get(getAdapterPosition());
                            String jsItem= new Gson().toJson(item);
                            Bundle bundle = new Bundle();
                            bundle.putString("item",jsItem);
                            Intent mIntent = new Intent(activity,ItemDetailActivity.class);
                            mIntent.putExtra("bundle",bundle);
                            activity.startActivity(mIntent);
                            break;
                        }



                    }

                }
            });


        }


    }

    @Override
    public ItemsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_items_row,viewGroup, false);
        mh = new ItemsAdapter.ViewHolder(v);

        return mh;
    }

    @Override
    public void onBindViewHolder(ItemsAdapter.ViewHolder holder, int i) {
        Item item = itemListFiltered.get(i);
        holder.tvName.setText(item.getItemName());
        holder.tvCode.setText(item.getItemCode());
        holder.tvQuantity.setText(String.valueOf(item.getItemQuantity()));
        Glide.with(activity)
                .load(item.getItemImageUrl())
                .into(holder.ivItem);






        setAnimation(holder.itemView, i);


    }



    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(activity, R.anim.slide_up);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }






    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    itemListFiltered = itemList;
                } else {
                    List<Item> filteredList = new ArrayList<>();
                    for (Item row : itemList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getItemName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    itemListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = itemListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                itemListFiltered = (ArrayList<Item>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
    @Override
    public int getItemCount() {
        return (itemListFiltered.size());
    }





    public void openAddPurchaseDialog(final String name){

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        final LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_add_purchased_product, null);
        dialogBuilder.setView(dialogView);


        pItemName = dialogView.findViewById(R.id.item_name);
        pItemPrice =  dialogView.findViewById(R.id.item_price);
        pItemQuantity =  dialogView.findViewById(R.id.item_quantity);


        pItemName.setText(name);
        pItemName.setInputType(InputType.TYPE_NULL);



        dialogBuilder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();




            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        b = dialogBuilder.create();
        b.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        b.show();
        dialogBuilder.setMessage(null);

        b.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {


                if(!TextUtils.isEmpty(pItemName.getText().toString()) && !TextUtils.isEmpty(pItemQuantity.getText().toString())) {
                    final Item item = itemListFiltered.get(position);
                    String url = item.itemImageUrl;
                    String code = item.itemCode;
                    int stock = item.itemQuantity;
                    itemListFiltered.remove(item);
                    notifyDataSetChanged();

                    Intent intent = new Intent("cart-message");
                    intent.putExtra("mode", 1);
                    intent.putExtra("item_code", code);
                    intent.putExtra("item_name", pItemName.getText().toString());
                    intent.putExtra("item_price", Integer.valueOf(pItemPrice.getText().toString()));
                    intent.putExtra("item_quantity", Integer.valueOf(pItemQuantity.getText().toString()));
                    intent.putExtra("stock_quantity", stock);
                    intent.putExtra("item_image_url", url);
                    LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
                    b.dismiss();
                }
                else{
                    pItemPrice.setError("Please enter a value");
                    pItemQuantity.setError("Please enter a value");

                }

            }
        });




    }



    public void openAddSalesDialog(final String name){

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        final LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_add_sales_product, null);
        dialogBuilder.setView(dialogView);


        final Item item = itemListFiltered.get(position);

        pItemName = dialogView.findViewById(R.id.item_name);

        pItemQuantity =  dialogView.findViewById(R.id.item_quantity);

        pItemQuantity.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String strQuantity = pItemQuantity.getText().toString();
                if(TextUtils.isEmpty(strQuantity)){
                    pItemQuantity.setError("Please Enter a Value");
                    return;
                }

                int tempQuantity = Integer.parseInt(strQuantity);
                if(tempQuantity>item.itemQuantity){
                    //Toast.makeText(activity, "It must be less than or equal to "+item.itemQuantity, Toast.LENGTH_LONG).show();
                    CustomToast.showToast(activity, activity.getResources().getDrawable(R.drawable.ic_info_white), "It must be less than or equal to "+item.itemQuantity);

                }

            }
        });

        //pItemQuantity.setFilters(new InputFilter[]{ new InputFilterMinMax("1", String.valueOf(item.itemQuantity))});

        pItemName.setText(name);
        pItemName.setInputType(InputType.TYPE_NULL);

        dialogBuilder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        b = dialogBuilder.create();
        b.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        b.show();
        dialogBuilder.setMessage(null);

        b.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String quantity=pItemQuantity.getText().toString();
                if(TextUtils.isEmpty(quantity)){
                    pItemQuantity.setError("Please Enter a Value");
                    return;
                }

                if(!TextUtils.isEmpty(quantity) && item.itemQuantity>=Integer.parseInt(quantity)) {
                    String url = item.itemImageUrl;
                    String code = item.itemCode;
                    int stock = item.itemQuantity;
                    itemListFiltered.remove(item);
                    notifyDataSetChanged();

                    Intent intent = new Intent("cart-message");
                    intent.putExtra("item_code", code);
                    intent.putExtra("item_name", pItemName.getText().toString());
                    intent.putExtra("item_quantity", Integer.valueOf(pItemQuantity.getText().toString()));
                    intent.putExtra("item_selling_price",item.openingItemQuantityVal);
                    intent.putExtra("stock_quantity", stock);
                    intent.putExtra("item_image_url", url);
                    LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
                    b.dismiss();
                }
                else{
                    CustomToast.showToast(activity, activity.getResources().getDrawable(R.drawable.ic_info_white), "It must be less than or equal to "+item.itemQuantity);
                }

            }
        });

    }
}