package com.deltatechnepal.deltaezinventory.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.deltatechnepal.deltaezinventory.R;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import com.deltatechnepal.deltaezinventory.model.Item;
import com.deltatechnepal.deltaezinventory.model.PurchaseCart;
import com.deltatechnepal.deltaezinventory.model.pCart;


public class PurchaseCartAdapter extends RecyclerView.Adapter<PurchaseCartAdapter.ViewHolder>{
    private Activity activity;
    private List<pCart> itemList;
    private List<PurchaseCart> purchaseList;
    EditText pItemName,pItemPrice,pItemQuantity;
    int position;

    private View v;
    private PurchaseCartAdapter.ViewHolder mh;

    public PurchaseCartAdapter(Activity activity, List<pCart> itemList,List<PurchaseCart> purchaseList) {
        this.activity = activity;
        this.itemList = itemList;
        this.purchaseList=purchaseList;

        /* bgColors = activity.getApplicationContext().getResources().getStringArray(R.array.movie_serial_bg);*/
    }
    public class ViewHolder extends RecyclerView.ViewHolder  {

        protected TextView tvName,tvPrice,tvQuantity;
        public ImageView ivItem;
        protected ViewGroup transitionsContainer;
        public LinearLayout viewBackground;
        public ConstraintLayout viewForeground;






        public ViewHolder(View view) {

            super(view);
            viewBackground = view.findViewById(R.id.viewBackground);
            viewForeground = view.findViewById(R.id.viewForeground);
            this.transitionsContainer = view.findViewById(R.id.top_holder);
            this.tvName = view.findViewById(R.id.itemName);
            this.tvPrice = view.findViewById(R.id.itemPrice);
            this.tvQuantity = view.findViewById(R.id.itemQuantity);
            this.ivItem = view.findViewById(R.id.itemImage);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    position=getAdapterPosition();
                    String name=itemList.get(position).getItemName();
                    String code=itemList.get(position).getItemCode();
                    int quantity=itemList.get(position).getStockQuantity();
                    String url=itemList.get(position).getItemImageUrl();

                    removeFromCartDialog(name,code,quantity,url);

                }
            });


        }


    }

    @Override
    public PurchaseCartAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_add_purchased_items,viewGroup, false);
        mh = new PurchaseCartAdapter.ViewHolder(v);

        return mh;
    }

    @Override
    public void onBindViewHolder(PurchaseCartAdapter.ViewHolder holder, int i) {
        pCart item = itemList.get(i);
        holder.tvName.setText(item.getItemName());
        holder.tvPrice.setText("Rs. "+getFormatedAmount(item.getItemPrice()));
        holder.tvQuantity.setText(String.valueOf(item.getItemQuantity()));
        Glide.with(activity)
                .load(item.getItemImageUrl())
                .into(holder.ivItem);









    }

    @Override
    public int getItemCount() {
        return (itemList.size());
    }


    private String getFormatedAmount(int amount){
        return NumberFormat.getNumberInstance(Locale.US).format(amount);
    }





    public void removeFromCartDialog(final String name,final String code,final int quantity,final String url){

        new AlertDialog.Builder(activity)
                .setTitle("Remove")
                .setMessage("Remove from Cart?")
                .setIcon(R.drawable.ic_info)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        pCart item = itemList.get(position);
                        itemList.remove(item);
                        PurchaseCart purchaseCart=purchaseList.get(position);
                        purchaseList.remove(purchaseCart);
                        notifyDataSetChanged();
                        Intent intent = new Intent("cart-message");
                        intent.putExtra("mode",0);
                        intent.putExtra("item_code",code);
                        intent.putExtra("item_name",name);
                        intent.putExtra("item_price",item.getItemPrice());
                        intent.putExtra("item_quantity",item.getItemQuantity());
                        intent.putExtra("stock_quantity",quantity);
                        intent.putExtra("item_image_url",url);
                        LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
                    }})
                .setNegativeButton("No", null).show();





    }





}