package com.deltatechnepal.deltaezinventory.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.deltatechnepal.deltaezinventory.R;
import com.deltatechnepal.deltaezinventory.model.DashMenu;

import java.util.List;


public class DashMenuAdapter extends BaseAdapter{
    private Context mContext;
    private List<DashMenu> menuList;
    private LayoutInflater inflater;

    public DashMenuAdapter(Context context, List<DashMenu> menuList) {
        this.mContext = context;
        this.menuList = menuList;
    }

    @Override
    public int getCount() {
        return menuList.size();
    }

    @Override
    public Object getItem(int i) {
        return menuList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DashMenu menu = menuList.get(position);
        if (inflater == null) inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) convertView = inflater.inflate(R.layout.grid_item_dash, null);
        TextView tvTitle =  convertView.findViewById(R.id.tvTitle);
        ImageView ivMenu = convertView.findViewById(R.id.ivMenu);
        tvTitle.setText(menuList.get(position).title);
        ivMenu.setImageResource(menuList.get(position).drawable);
        return convertView;
    }
}
