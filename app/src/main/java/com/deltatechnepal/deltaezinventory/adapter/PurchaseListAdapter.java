package com.deltatechnepal.deltaezinventory.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.deltatechnepal.deltaezinventory.R;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.deltatechnepal.deltaezinventory.helper.DateConverter;
import com.deltatechnepal.deltaezinventory.helper.MFunction;
import com.deltatechnepal.deltaezinventory.main.PurchaseCartActivity;
import com.deltatechnepal.deltaezinventory.model.Item;
import com.deltatechnepal.deltaezinventory.model.Purchase;
import com.deltatechnepal.deltaezinventory.model.PurchaseCart;
import com.deltatechnepal.deltaezinventory.model.pCart;


public class PurchaseListAdapter extends RecyclerView.Adapter<PurchaseListAdapter.ViewHolder>{
    private Activity activity;
    private List<Purchase> purchaseList;
    EditText pItemName,pItemPrice,pItemQuantity;
    int position;
    private int lastPosition=-1;
    AlertDialog b;
    private View v;
    private PurchaseListAdapter.ViewHolder mh;

    public PurchaseListAdapter(Activity activity, List<Purchase> purchaseList) {
        this.activity = activity;
        this.purchaseList = purchaseList;

    }
    public class ViewHolder extends RecyclerView.ViewHolder  {

        protected TextView tvPurchaseNumber,tvPurchaseDate;
        public ImageView ivItem;
        protected ViewGroup transitionsContainer;




        public ViewHolder(View view) {

            super(view);

            this.transitionsContainer = view.findViewById(R.id.top_holder);
            this.tvPurchaseNumber = view.findViewById(R.id.purchaseNumber);
            this.tvPurchaseDate = view.findViewById(R.id.purchaseDate);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    position=getAdapterPosition();
                    openBottomSheet(position);

                    }
            });


        }


    }

    @Override
    public PurchaseListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_purchase_cart,viewGroup, false);
        mh = new PurchaseListAdapter.ViewHolder(v);

        return mh;
    }

    @Override
    public void onBindViewHolder(PurchaseListAdapter.ViewHolder holder, int i) {
        Purchase purchase = purchaseList.get(i);
        String date = MFunction.getDate(purchase.getPurchaseDate());
        holder.tvPurchaseNumber.setText("Purchase Number : "+String.valueOf((getItemCount()-i)));
        holder.tvPurchaseDate.setText(date);
        setAnimation(holder.itemView, i);


    }



    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(activity, R.anim.slide_up);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }





    @Override
    public int getItemCount() {
        return (purchaseList.size());
    }


    private String getFormatedAmount(long amount){
        return NumberFormat.getNumberInstance(Locale.US).format(amount);
    }




    public void openBottomSheet(int position) {


        View view = activity.getLayoutInflater().inflate(R.layout.bottom_sheet_purchased_cart, null);

        BottomSheetDialog dialog = new BottomSheetDialog(activity,R.style.BaseBottomSheetDialog);
        dialog.setContentView(view);
        dialog.show();

        Purchase purchase=purchaseList.get(position);
        TextView purchasedNumber = view.findViewById(R.id.tvPurchasedNumber);
        purchasedNumber.setText("Purchase Number: "+String.valueOf((getItemCount()-position)));

        Animation animation= AnimationUtils.loadAnimation(activity,
                R.anim.slide_right);
        animation.setDuration(700);

        TextView date = view.findViewById(R.id.tvPurchasedDate);
        date.setText("Purchased Date: "+DateConverter.getInstance(activity).getDate(purchase.getPurchaseDate()));
        date.startAnimation(animation);


        TextView totalPrice =  view.findViewById(R.id.tvTotalBillValue);
        totalPrice.setText("Total Price: Rs."+String.valueOf(getFormatedAmount(purchase.getTotalPrice())));
        totalPrice.startAnimation(animation);

        RecyclerView rvCart = view.findViewById(R.id.rvPurchasedCart);
        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(activity);
        rvCart.setLayoutManager(linearLayoutManager);
        rvCart.setHasFixedSize(true);
        rvCart.setNestedScrollingEnabled(true);

        List<PurchaseCart> purchaseCart =new ArrayList<>();
        purchaseCart = purchase.getPurchaseCart();


        PurchasedCartAdapter cartAdapter=new PurchasedCartAdapter(activity,purchaseCart);
        rvCart.setAdapter(cartAdapter);

        }






}