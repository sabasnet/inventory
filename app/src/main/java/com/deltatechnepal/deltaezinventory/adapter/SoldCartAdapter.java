package com.deltatechnepal.deltaezinventory.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.deltatechnepal.deltaezinventory.R;
import com.deltatechnepal.deltaezinventory.model.SalesCart;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;


public class SoldCartAdapter extends RecyclerView.Adapter<SoldCartAdapter.ViewHolder>{
    private Activity activity;
    private List<SalesCart> itemList;
    private int lastPosition=-1;
    private View v;
    private SoldCartAdapter.ViewHolder mh;

    public SoldCartAdapter(Activity activity, List<SalesCart> itemList) {
        this.activity = activity;
        this.itemList = itemList;

        /* bgColors = activity.getApplicationContext().getResources().getStringArray(R.array.movie_serial_bg);*/
    }
    public class ViewHolder extends RecyclerView.ViewHolder  {

        protected TextView tvName,tvQuantity;
        public ImageView ivItem;
        protected ViewGroup transitionsContainer;




        public ViewHolder(View view) {

            super(view);

            this.transitionsContainer = view.findViewById(R.id.top_holder);
            this.tvName = view.findViewById(R.id.itemName);
            this.tvQuantity = view.findViewById(R.id.itemQuantity);
            this.ivItem = view.findViewById(R.id.itemImage);




        }


    }

    @Override
    public SoldCartAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_add_sales_items,viewGroup, false);
        mh = new SoldCartAdapter.ViewHolder(v);

        return mh;
    }

    @Override
    public void onBindViewHolder(SoldCartAdapter.ViewHolder holder, int i) {
        SalesCart item = itemList.get(i);
        holder.tvName.setText(item.getItemName());
        holder.tvQuantity.setText(String.valueOf(item.getItemQuantity()));
        Glide.with(activity)
                .load(item.getItemImageUrl())
                .into(holder.ivItem);



        setAnimation(holder.itemView, i);





    }


    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(activity, R.anim.slide_up);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }


    @Override
    public int getItemCount() {
        return (itemList.size());
    }













}