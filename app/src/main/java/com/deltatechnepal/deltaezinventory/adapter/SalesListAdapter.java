package com.deltatechnepal.deltaezinventory.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.util.Log;
import android.widget.TextView;

import com.deltatechnepal.deltaezinventory.R;
import com.deltatechnepal.deltaezinventory.helper.DateConverter;
import com.deltatechnepal.deltaezinventory.helper.SharedPreManager;
import com.deltatechnepal.deltaezinventory.main.SalesActivity;
import com.deltatechnepal.deltaezinventory.model.Purchase;
import com.deltatechnepal.deltaezinventory.model.PurchaseCart;
import com.deltatechnepal.deltaezinventory.model.Sales;
import com.deltatechnepal.deltaezinventory.model.SalesCart;
import com.deltatechnepal.deltaezinventory.model.sCart;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class SalesListAdapter extends RecyclerView.Adapter<SalesListAdapter.ViewHolder>{
    private Activity activity;
    private String TAG = SalesListAdapter.class.getSimpleName();
    private List<Sales> salesList;
    EditText pItemName,pItemPrice,pItemQuantity;
    int position;
    private int lastPosition=-1;
    AlertDialog b;
    private View v;
    private SalesListAdapter.ViewHolder mh;
    BottomSheetDialog dialog;

    public SalesListAdapter(Activity activity, List<Sales> salesList) {
        this.activity = activity;
        this.salesList = salesList;

    }
    public class ViewHolder extends RecyclerView.ViewHolder  {

        protected TextView tvSalesNumber,tvSalesDate;
        public ImageView ivItem;
        protected ViewGroup transitionsContainer;




        public ViewHolder(View view) {

            super(view);

            this.transitionsContainer = view.findViewById(R.id.top_holder);
            this.tvSalesNumber = view.findViewById(R.id.purchaseNumber);
            this.tvSalesDate = view.findViewById(R.id.purchaseDate);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    position=getAdapterPosition();
                    openBottomSheet(position);

                    }
            });


        }


    }

    @Override
    public SalesListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_purchase_cart,viewGroup, false);
        mh = new SalesListAdapter.ViewHolder(v);

        return mh;
    }

    @Override
    public void onBindViewHolder(SalesListAdapter.ViewHolder holder, int i) {
        Sales sale = salesList.get(i);
        String date = DateConverter.getInstance(activity).getDate(sale.getSalesDate());
        holder.tvSalesNumber.setText("Sales Number : "+String.valueOf((getItemCount()-i)));
        holder.tvSalesDate.setText(date);
        setAnimation(holder.itemView, i);


    }



    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(activity, R.anim.slide_up);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }





    @Override
    public int getItemCount() {
        return (salesList.size());
    }


    private String getFormatedAmount(long amount){
        return NumberFormat.getNumberInstance(Locale.US).format(amount);
    }




    public void openBottomSheet(final int position) {


        View view = activity.getLayoutInflater().inflate(R.layout.bottom_sheet_sales_cart, null);
        dialog = new BottomSheetDialog(activity, R.style.BaseBottomSheetDialog);
        dialog.setContentView(view);
        dialog.show();

        final Sales sale = salesList.get(position);
        TextView salesNumber = view.findViewById(R.id.tvSalesNumber);
        salesNumber.setText("Sales Number: " + String.valueOf((getItemCount()-position)));

        Animation animation = AnimationUtils.loadAnimation(activity,
                R.anim.slide_right);
        animation.setDuration(700);

        TextView date = view.findViewById(R.id.tvSalesDate);
        date.setText("Sales Date: " + DateConverter.getInstance(activity).getDate(sale.getSalesDate()));
        date.startAnimation(animation);


        RecyclerView rvCart = view.findViewById(R.id.rvSalesCart);
        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(activity);
        rvCart.setLayoutManager(linearLayoutManager);
        rvCart.setHasFixedSize(true);
        rvCart.setNestedScrollingEnabled(true);

        List<SalesCart> salesCart = new ArrayList<>();
        salesCart = sale.getSalesCart();


        SoldCartAdapter cartAdapter = new SoldCartAdapter(activity, salesCart);
        rvCart.setAdapter(cartAdapter);


        final Button delete = view.findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(activity)
                        .setTitle("Delete")
                        .setMessage("Are you sure you want to delete this record?")
                        .setIcon(R.drawable.ic_info)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                deleteSales(sale);

                            }})
                        .setNegativeButton("No", null).show();


            }
        });
    }



    public void deleteSales(Sales sale){

        String number = SharedPreManager.getInstance(activity).getUser().getMobileNumber();
        //Firebase
        FirebaseDatabase mFirebaseInstance = FirebaseDatabase.getInstance();

        // get reference to 'users' node
        final DatabaseReference mFirebaseDatabase = mFirebaseInstance.getReference
                (number + "/Book/Sales/");


        final Query delete = mFirebaseDatabase.orderByChild("salesCode").equalTo(sale.getSalesCode());

        delete.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    postSnapshot.getRef().removeValue();
                    dialog.hide();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled", databaseError.toException());
            }
        });


    }


        }