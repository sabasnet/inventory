package com.deltatechnepal.deltaezinventory.adapter;


import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.deltatechnepal.deltaezinventory.R;

import java.util.ArrayList;
import java.util.List;

import com.deltatechnepal.deltaezinventory.model.Item;


public class AddPurchaseAdapter extends RecyclerView.Adapter<AddPurchaseAdapter.ViewHolder> implements Filterable {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Item> itemList;
    private List<Item> itemListFiltered;
    EditText pItemName,pItemPrice,pItemQuantity;
    int mode;
    AlertDialog b;

    private View v;
    private AddPurchaseAdapter.ViewHolder mh;

    public AddPurchaseAdapter(Activity activity, List<Item> itemList,int mode) {
        this.activity = activity;
        this.mode=mode;
        this.itemList = itemList;
        this.itemListFiltered = itemList;
        /* bgColors = activity.getApplicationContext().getResources().getStringArray(R.array.movie_serial_bg);*/
    }
    public class ViewHolder extends RecyclerView.ViewHolder  {

        protected TextView tvName,tvCode,tvQuantity;
        public ImageView ivItem;
        protected ViewGroup transitionsContainer;




        public ViewHolder(View view) {

            super(view);

            this.transitionsContainer = view.findViewById(R.id.top_holder);
            this.tvName = view.findViewById(R.id.itemName);
            this.tvCode = view.findViewById(R.id.itemCode);
            this.tvQuantity = view.findViewById(R.id.itemQuantity);
            this.ivItem = view.findViewById(R.id.itemImage);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback

                    switch (mode){

                        case 2:
                            openAddPurchaseDialog(itemListFiltered.get(getAdapterPosition()).getItemName());
                            break;



                    }

                }
            });


        }


    }

    @Override
    public AddPurchaseAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_items_row,viewGroup, false);
        mh = new AddPurchaseAdapter.ViewHolder(v);

        return mh;
    }

    @Override
    public void onBindViewHolder(AddPurchaseAdapter.ViewHolder holder, int i) {
        Item item = itemListFiltered.get(i);
        holder.tvName.setText(item.getItemName());
        holder.tvCode.setText(item.getItemCode());
        holder.tvQuantity.setText(String.valueOf(item.getItemQuantity()));
        Glide.with(activity)
                .load(item.getItemImageUrl())
                .into(holder.ivItem);









    }
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    itemListFiltered = itemList;
                } else {
                    List<Item> filteredList = new ArrayList<>();
                    for (Item row : itemList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getItemName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    itemListFiltered = filteredList;


                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = itemListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                itemListFiltered = (ArrayList<Item>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
    @Override
    public int getItemCount() {
        return (itemListFiltered.size());
    }





    public void openAddPurchaseDialog(String name){

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_add_purchased_product, null);
        dialogBuilder.setView(dialogView);




        pItemName = dialogView.findViewById(R.id.item_name);
        pItemPrice =  dialogView.findViewById(R.id.item_price);
        pItemQuantity =  dialogView.findViewById(R.id.item_quantity);


        pItemName.setText(name);



        dialogBuilder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
            }
        });

        b = dialogBuilder.create();
        b.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        b.show();
        dialogBuilder.setMessage(null);






    }





}