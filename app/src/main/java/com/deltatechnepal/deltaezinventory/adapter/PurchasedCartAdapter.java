package com.deltatechnepal.deltaezinventory.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.deltatechnepal.deltaezinventory.R;
import com.deltatechnepal.deltaezinventory.model.PurchaseCart;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;


public class PurchasedCartAdapter extends RecyclerView.Adapter<PurchasedCartAdapter.ViewHolder>{
    private Activity activity;
    private List<PurchaseCart> itemList;
    EditText pItemName,pItemPrice,pItemQuantity;
    int position;
    private int lastPosition=-1;
    private View v;
    private PurchasedCartAdapter.ViewHolder mh;

    public PurchasedCartAdapter(Activity activity, List<PurchaseCart> itemList) {
        this.activity = activity;
        this.itemList = itemList;

        /* bgColors = activity.getApplicationContext().getResources().getStringArray(R.array.movie_serial_bg);*/
    }
    public class ViewHolder extends RecyclerView.ViewHolder  {

        protected TextView tvName,tvPrice,tvQuantity;
        public ImageView ivItem;
        protected ViewGroup transitionsContainer;




        public ViewHolder(View view) {

            super(view);

            this.transitionsContainer = view.findViewById(R.id.top_holder);
            this.tvName = view.findViewById(R.id.itemName);
            this.tvPrice = view.findViewById(R.id.itemPrice);
            this.tvQuantity = view.findViewById(R.id.itemQuantity);
            this.ivItem = view.findViewById(R.id.itemImage);




        }


    }

    @Override
    public PurchasedCartAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_add_purchased_items,viewGroup, false);
        mh = new PurchasedCartAdapter.ViewHolder(v);

        return mh;
    }

    @Override
    public void onBindViewHolder(PurchasedCartAdapter.ViewHolder holder, int i) {
        PurchaseCart item = itemList.get(i);
        holder.tvName.setText(item.getItemName());
        holder.tvPrice.setText("Rs. "+getFormatedAmount(item.getItemPrice()));
        holder.tvQuantity.setText(String.valueOf(item.getItemQuantity()));
        Glide.with(activity)
                .load(item.getItemImageUrl())
                .into(holder.ivItem);



        setAnimation(holder.itemView, i);





    }


    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(activity, R.anim.slide_up);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }


    @Override
    public int getItemCount() {
        return (itemList.size());
    }


    private String getFormatedAmount(int amount){
        return NumberFormat.getNumberInstance(Locale.US).format(amount);
    }










}