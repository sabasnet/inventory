package com.deltatechnepal.deltaezinventory.helper;
import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateConverter {

    Date date;
    SimpleDateFormat formatter;
    Calendar mcurrentDate;
    String formattedDate;
    private static  DateConverter mInstance;
    private  Context mCtx;


    private DateConverter(Context context) {
        mCtx = context;
    }
    public static synchronized DateConverter getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DateConverter(context);
        }
        return mInstance;
    }

    public String getCurrentDate(){
        formatter = new SimpleDateFormat("yyyy/MM/dd");
        mcurrentDate = Calendar.getInstance();
        String currentDate=formatter.format(mcurrentDate.getTime());

        return currentDate;
    }
    public String getCurrentDateinMilli(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        formattedDate= sdf.format(new Date());

        try {
            date = sdf.parse(formattedDate);
        }
        catch (Exception e){

        }



        return String.valueOf(date.getTime());



    }
    public long getCurrentDateinMilliseonds(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        formattedDate= sdf.format(new Date());

        try {
            date = sdf.parse(formattedDate);
        }
        catch (Exception e){

        }



        return date.getTime();



    }

    public String getDate(long milliSeconds)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public long getMilli(String paramDate)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        try {
            date = sdf.parse(paramDate);
        }
        catch (Exception e){

        }
        return date.getTime();
    }


}
