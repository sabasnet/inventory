package com.deltatechnepal.deltaezinventory.helper;


import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.deltatechnepal.deltaezinventory.R;

public class CustomToast {


    public static void showToast(Activity activity, Drawable drawable, String message) {


        LayoutInflater inflater = activity.getLayoutInflater();

        View view = inflater.inflate(R.layout.custom_toast, null);

        TextView tv = view.findViewById(R.id.toastMessage);
        ImageView iv = view.findViewById(R.id.toastIcon);
        Toast toast = new Toast(activity);
        iv.setImageDrawable(drawable);
        tv.setText(message);
        toast.setGravity(Gravity.BOTTOM, 0, 100);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(view);
        toast.show();

    }
}
