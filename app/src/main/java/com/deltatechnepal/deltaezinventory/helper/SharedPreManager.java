package com.deltatechnepal.deltaezinventory.helper;



import android.content.Context;
import android.content.SharedPreferences;

import com.deltatechnepal.deltaezinventory.model.User;
import com.google.firebase.auth.FirebaseAuth;


public class SharedPreManager {

    //the constants
    private static final String SHARED_PREF_NAME = "userlogin";
    private static final String KEY_USERID = "keyuserid";
    private static final String KEY_MOBILE_NUMBER = "keynumber";

    private static  SharedPreManager mInstance;
    private  Context mCtx;
    private FirebaseAuth mAuth;
    private SharedPreManager(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPreManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPreManager(context);
        }
        return mInstance;
    }

    //method to let the user login
    //this method will store the user data in shared preferences
    public void userLogin(User user) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_USERID, user.getUserId());
        editor.putString(KEY_MOBILE_NUMBER, user.getMobileNumber());
        editor.apply();
    }


    public void sortItemValue(int value) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("sortList", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("sortBy",value);
        editor.apply();
    }

    public void sortOrder(boolean value) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("sortList", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("sortOrder",value);
        editor.apply();
    }


    //this method will checker whether user is already logged in or not
    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USERID, null) != null;
    }


    public int getSortValue() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("sortList", Context.MODE_PRIVATE);
        return sharedPreferences.getInt("sortBy", 1);
    }


    public boolean isSortReversed() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("sortList", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("sortOrder",false);
    }


    //this method will give the logged in user
    public User getUser() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new User(
                sharedPreferences.getString(KEY_USERID, ""),
                sharedPreferences.getString(KEY_MOBILE_NUMBER, "")
        );
    }

    //this method will logout the user
    public void logout() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();

        mAuth = FirebaseAuth.getInstance();
        mAuth.signOut();


    }
}